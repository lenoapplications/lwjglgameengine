package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.TerrainNormalsModel;

public class UniquePerpendicularNormals extends TerrainNormalsModel {

    public UniquePerpendicularNormals(int size) {
        super(size);
    }

    public void generateNormals(int rowSize,int columnSize){
        for(int row = 0; row < rowSize; ++row){
            for(int column = 0; column < columnSize; ++column){
                addNormals(0f,1f,0f);
                addNormals(0f,1f,0f);
                addNormals(0f,1f,0f);
                addNormals(0f,1f,0f);
            }
        }
    }
}

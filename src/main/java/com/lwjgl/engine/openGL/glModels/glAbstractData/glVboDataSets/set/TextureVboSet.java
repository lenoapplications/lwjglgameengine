package com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets.set;

import org.newdawn.slick.opengl.PNGDecoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_TEXTURE_MAX_LEVEL;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class TextureVboSet extends GlVboDataAbstract {
    private final String resFolder = "./res/assets/";
    private int textureId;

    public TextureVboSet(int sizeOfData){
        super(1,sizeOfData);
        this.textureId = -1;
    }

    public void setupTextureImg(int textureId,String filename)  {
        PNGDecoder decoder = decodeImage(filename);
        ByteBuffer buf = saveToBuffer(decoder);

        this.textureId = textureId;
        glBindTexture(GL_TEXTURE_2D,textureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,decoder.getWidth(),decoder.getHeight(),0,GL_RGBA,GL_UNSIGNED_BYTE,buf);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    }

    public void setupTextureImg(int textureId,String filename,int mipmapLevel)  {
        PNGDecoder decoder = decodeImage(filename);
        ByteBuffer buf = saveToBuffer(decoder);

        this.textureId = textureId;
        glBindTexture(GL_TEXTURE_2D,textureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,decoder.getWidth(),decoder.getHeight(),0,GL_RGBA,GL_UNSIGNED_BYTE,buf);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipmapLevel);
    }

    public int setupTextureImg(String filename){
        int generatedTextureId = glGenTextures();
        PNGDecoder decoder = decodeImage(filename);
        ByteBuffer buf = saveToBuffer(decoder);

        glBindTexture(GL_TEXTURE_2D,generatedTextureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,decoder.getWidth(),decoder.getHeight(),0,GL_RGBA,GL_UNSIGNED_BYTE,buf);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);

        return generatedTextureId;
    }

    public void setupTextureImage(String filename,int mipmapLevel){
        int generatedTextureId = glGenTextures();
        PNGDecoder decoder = decodeImage(filename);
        ByteBuffer buf = saveToBuffer(decoder);

        glBindTexture(GL_TEXTURE_2D,generatedTextureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,decoder.getWidth(),decoder.getHeight(),0,GL_RGBA,GL_UNSIGNED_BYTE,buf);
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipmapLevel);

        this.textureId = generatedTextureId;
    }

    public int getTextureId() {
        return textureId;
    }




    private PNGDecoder decodeImage(String filename){
        try {
            PNGDecoder decoder = new PNGDecoder(new FileInputStream(new File(resFolder.concat(filename))));
            return decoder;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    private ByteBuffer saveToBuffer(PNGDecoder decoder){
        try {
            ByteBuffer buf = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buf,decoder.getWidth() * 4, PNGDecoder.RGBA);
            buf.flip();
            return buf;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.TerrainVerticesModel;

import java.util.Random;

public class SharedVerticesTerrain extends TerrainVerticesModel {

    public SharedVerticesTerrain(int rowVertices, int columnVertices,float xPosition,float yPosition,float zPosition,float terrainWidth,float terrainDepth) {
        super(rowVertices, columnVertices,xPosition,yPosition,zPosition,terrainWidth,terrainDepth,terrainWidth/(columnVertices -1f),terrainDepth/(rowVertices-1f));
    }


    public void generateTerrainModel(){
        for(int row = 0; row < rowVertices; ++row){
            for(int column = 0; column < columnVertices; ++column){
                float x = xPosition + (column * widthOfSquare);
                float y = new Random().nextFloat()/5;
                float z = zPosition + (row * depthOfSquare);
                addVertice(x,y,z);
            }
        }
    }

}

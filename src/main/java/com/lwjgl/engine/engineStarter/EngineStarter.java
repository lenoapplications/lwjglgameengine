package com.lwjgl.engine.engineStarter;

import com.lwjgl.engine.engineLoop.EngineLoop;
import com.lwjgl.engine.engineLoop.loopModels.FrameRunner;
import com.lwjgl.engine.gameObjects.holder.GameObjectHolder;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.camerTypes.ThirdPersonCamera;
import com.lwjgl.engine.gui.window.Window;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.openGL.glModels.glRenderer.GlRenderer;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderController.GlShaderController;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;

import static org.lwjgl.glfw.GLFW.*;

public class EngineStarter {

    private class DefaultFrameRunner implements FrameRunner{
        private final GameObjectHolder gameObjectHolder;
        private final GlRenderer glRenderer;

        public DefaultFrameRunner(){
            gameObjectHolder = EngineComponentsSharedObject.getEngineComponentSharedObject().getGameObjectHolder();
            glRenderer = new GlRenderer(gameObjectHolder);
        }

        @Override
        public void runFrame() {
            glRenderer.objectsRender();
        }
    }

    public EngineStarter(){

    }


    public void setGlShadersControllerSize(int size){
        new GlShaderController(size);
    }

    public void initEngine(int width, int height, String title){
        Window window = new Window();
        window.initWindow(width,height,title);

        new GameObjectHolder(setupCameraGlInputListener());
        new EngineLoop(new EngineStarter.DefaultFrameRunner()).initLoop();
    }

    public void startEngine(){
        EngineComponentsSharedObject.getEngineComponentSharedObject().getActiveEngineLoop().runEngineLoop();
    }

    public void stopEngine(){
        EngineComponentsSharedObject.getEngineComponentSharedObject().getActiveWindow().close();
    }

    private GlInputListener setupCameraGlInputListener(){
        return new GlInputListener() {
            @Override
            public void listenForInputs(CameraModel camera, GlInputHandler glInputHandler) {
                ((ThirdPersonCamera)camera).increaseDistanceFromPlayer(glInputHandler.getScrollYOffset());


                if(glInputHandler.isMouseButtonPressed(GLFW_MOUSE_BUTTON_1)){
                    ((ThirdPersonCamera)camera).increasePitch((float) glInputHandler.getYOffset());
                    ((ThirdPersonCamera)camera).increaseAngleAroundPlayer((float) glInputHandler.getXOffset());
                }
            }
        };
    }
}

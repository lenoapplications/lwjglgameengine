package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.ComponentModel;

public abstract class TerrainIndicesModel extends ComponentModel<Integer> {

    public TerrainIndicesModel(int size, int skipSize){
        super(new Integer[size * skipSize],skipSize);
    }

    public void addIndices(int...indexes){
        try {
            if(indexes.length != skipSize){
                throw new Exception("To much indexes, it requires "+skipSize);
            }
            for(int value : indexes){
                addNewValueToCurrentPointer(value);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public int[] getIndices(int index){
        int[] result = new int[skipSize];
        int realIndex = index * skipSize;

        for(int i = 0; i < skipSize ; ++i){
            result[i] = getValue(realIndex);
            ++realIndex;
        }
        return result;
    }
    public int[] getAllIndices(){
        int[] primitive = new int[realSizeOfArray()];
        int index = 0;
        for(int indices : getArray()){
            primitive[index] = indices;
            ++index;
        }
        return primitive;
    }


    @Override
    public String toString() {
        int index = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Indices\n\n");
        stringBuilder.append("( ");
        for(int v : getArray()){
            if (index % skipSize == 0 && index != 0){
                stringBuilder.append(")\n");
                stringBuilder.append("( ");
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }else{
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }
            ++index;
        }
        stringBuilder.append(") ");
        return stringBuilder.toString();
    }

}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.TerrainIndicesModel;

public class SharedVerticesTerrainIndices extends TerrainIndicesModel {
    public SharedVerticesTerrainIndices(int size) {
        super(size, 6);
    }


    public void generateSharedIndices(int rowVertices,int columnVertices){
        for(int row = 0; row < rowVertices - 1 ; ++row){
            for(int column = 0; column < columnVertices -1 ; ++column){
                int leftDownPoint = ((row * columnVertices) + column);
                int rightDownPoint = leftDownPoint + 1;
                int rightUpPoint = rightDownPoint + columnVertices;
                int leftUpPoint = rightUpPoint - 1;

                addNewValueToCurrentPointer(leftDownPoint);
                addNewValueToCurrentPointer(rightDownPoint);
                addNewValueToCurrentPointer(rightUpPoint);
                addNewValueToCurrentPointer(rightUpPoint);
                addNewValueToCurrentPointer(leftUpPoint);
                addNewValueToCurrentPointer(leftDownPoint);
            }
        }
    }
}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.visualModel;

import com.lwjgl.engine.gameObjects.objects.objectModel.GameObjectModel;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public abstract class AbstractedGameObject implements GameObjectModel {
    protected final Matrix4f projectionMatrix;
    protected final Matrix4f worldMatrix;
    protected final Vector3f position;
    protected Vector3f rotation;


    protected final float FIELD_OF_VIEW = 90;
    protected final float Z_NEAR = 0.01f;
    protected final float Z_FAR = 300f;
    protected float shineDamper = 1f;
    protected float reflectivity = 0;
    protected float scale = 1f;

    public AbstractedGameObject(){
        this.projectionMatrix = new Matrix4f();
        this.worldMatrix = new Matrix4f();
        this.position = new Vector3f(0f,0f,0f);
        this.rotation = new Vector3f(0f,0f,0f);
        projectionMatrix.identity();
        worldMatrix.identity();
        setProjectionMatrix(640,480);

    }

    //PROJECTION MATRIX NE TREBA STALNO STAVLJATI JER SE ON NE MJENJA
    protected void loadUniforms(GlShaderAbstractProgram glShaderAbstractProgram){
        glShaderAbstractProgram.loadMatrix4f(glShaderAbstractProgram.WORLD_MATRIX,worldMatrix);
        glShaderAbstractProgram.loadMatrix4f(glShaderAbstractProgram.PROJECTION_MATRIX,projectionMatrix);
        glShaderAbstractProgram.loadFloat(glShaderAbstractProgram.SHINE_DAMPER,shineDamper);
        glShaderAbstractProgram.loadFloat(glShaderAbstractProgram.REFLECTIVITY,reflectivity);
    }


    public void changeXPosition(float x){
        position.x = x;
    }
    public void changeYPosition(float y){
        position.y = y;
    }
    public void changeZPosition(float z){
        position.z = z;
    }
    public void increaseXPosition(float x){
        position.x += x;
    }
    public void increaseYPosition(float y){
        position.y +=y;
    }
    public void increaseZPosition(float z){
        position.z +=z;
    }

    public void rotateX(float angle){
        this.rotation.x = angle;
    }
    public void rotateY(float angle){
        this.rotation.y = angle;
    }
    public void rotateZ(float angle){
        this.rotation.z = angle;
    }
    public void increaseXRotation(float angle){
        this.rotation.x += angle;

    }
    public void increaseYRotation(float angle){
        this.rotation.y += angle;
    }
    public void increaseZRotation(float angle){
        this.rotation.z += angle;
    }


    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
    public float getScale() {
        return scale;
    }
    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    protected void setProjectionMatrix(int width,int height) {
        projectionMatrix.setPerspectiveLH((float)Math.toRadians(FIELD_OF_VIEW),(float)width/height,Z_NEAR,Z_FAR);
    }
    public void setTransformationMatrixIdentity(){
        worldMatrix.identity();
    }

    public Matrix4f getWorldMatrix() {
        return worldMatrix;
    }


}

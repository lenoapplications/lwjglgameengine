package com.lwjgl.engine.gameObjects.holder;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.TerrainModel;

import java.util.ArrayList;
import java.util.List;

public class TerrainObjectHolder {
    private final List<TerrainModel> terrainModelList;

    public TerrainObjectHolder() {
        this.terrainModelList = new ArrayList<>();
    }

    public void addNewTerrain(TerrainModel terrainModel){
        terrainModelList.add(terrainModel);
    }

    public List<TerrainModel> getTerrainModelList() {
        return terrainModelList;
    }
}

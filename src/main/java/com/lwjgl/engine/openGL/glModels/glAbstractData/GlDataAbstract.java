package com.lwjgl.engine.openGL.glModels.glAbstractData;

import com.lwjgl.engine.openGL.glModels.glAbstractData.glDataAbstractEnabler.GlDataAbstractEnablerInterface;
import com.lwjgl.engine.openGL.glModels.glAbstractData.glRawDataObj.GlRawDataObj;
import com.lwjgl.engine.openGL.glModels.glAbstractData.glStatementAssist.GlStatementAssist;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class GlDataAbstract extends GlStatementAssist{
    private final boolean transparency;
    private GlDataAbstractEnablerInterface glDataAbstractEnablerInterface;


    public GlDataAbstract(boolean transparency){
        this.transparency = transparency;
    }

    public void initObjAndTexture(String objFilename, String textureFilename){
        GlRawDataObj glRawDataObj = new GlRawDataObj(objFilename);
        setupDataSet(glRawDataObj,textureFilename);
        generateVaoAndVbo(glRawDataObj);
        setupGlDataAbstractEnablerInterface(glRawDataObj);

    }
    public void initObjAndTexture(float[] vertices,float[] normals,float[] textureCoords,int[] indices,String textureFilename){
        GlRawDataObj glRawDataObj = new GlRawDataObj(true,true);
        glRawDataObj.setVerticesArray(vertices);
        glRawDataObj.setNormalsArray(normals);
        glRawDataObj.setTextureArray(textureCoords);
        glRawDataObj.setIndicesArray(indices);
        setupDataSet(glRawDataObj,textureFilename);

        generateVaoAndVbo(glRawDataObj);
        setupGlDataAbstractEnablerInterface(glRawDataObj);

    }
    public void initObjAndColor(float[] vertices,float[] normals,float[] colors,int[] indices){
        GlRawDataObj glRawDataObj = new GlRawDataObj(false,true);
        glRawDataObj.setVerticesArray(vertices);
        glRawDataObj.setNormalsArray(normals);
        glRawDataObj.setColorsArray(colors);
        glRawDataObj.setIndicesArray(indices);

        setupDataSet(glRawDataObj,null);
        generateVaoAndVbo(glRawDataObj);
        setupGlDataAbstractEnablerInterface(glRawDataObj);

    }
    public void initObjAndColor(float[] vertices,float[] colors,int[] indices){
        GlRawDataObj glRawDataObj = new GlRawDataObj(false,false);
        glRawDataObj.setVerticesArray(vertices);
        glRawDataObj.setColorsArray(colors);
        glRawDataObj.setIndicesArray(indices);

        setupDataSet(glRawDataObj,null);
        generateVaoAndVbo(glRawDataObj);
        setupGlDataAbstractEnablerInterface(glRawDataObj);
    }

    private void setupGlDataAbstractEnablerInterface(GlRawDataObj glRawDataObj){
        this.glDataAbstractEnablerInterface = glRawDataObj.createEnablerInterface(this);
    }

    private void generateVaoAndVbo(GlRawDataObj glRawDataObj){
        generateNewVaoId();
        bindVertexArray();

        bindVertices(glRawDataObj);
        bindTexture(glRawDataObj);
        bindColors(glRawDataObj);
        bindNormals(glRawDataObj);
        bindIndices(glRawDataObj);

        unbindVboArrayBuffer();
        unbindVboElementBuffer();
        unbindVertexArray();
    }

    private void bindVertexArrayAndIndices(){
        bindVertexArray();
        bindVboElementBuffer(getIndicesVboId());
    }

    private void enableCulling(){
        GL11.glEnable(GL_CULL_FACE);
        GL11.glCullFace(GL_FRONT);
    }

    private void enableVerticesAndTextureAttrib(){
        glDataAbstractEnablerInterface.enableVertex();
    }
    private void unbindAll(){
        unbindVboElementBuffer();
        unbindVertexArray();
    }


    private void setupDataSet(GlRawDataObj glRawDataObj,String textureFilename){
        setVertices(glRawDataObj.verticesSize());

        if (glRawDataObj.isUsingTexture()){
            setTexture(glRawDataObj.textureSize(),textureFilename);
            setColors();
        }else{
            setColors(glRawDataObj.colorsSize());
            setTexture();
        }

        if(glRawDataObj.isUsingNormals()){
            setNormals(glRawDataObj.normalsSize());
        }else{
            setNormals(0);
        }

        setIndices(glRawDataObj.indicesSize());
    }


    private void bindVertices(GlRawDataObj glRawDataObj){
        bindVboArrayBuffer(getVerticesVboId());
        bufferArrayData(glRawDataObj.getVerticesArray(),3,getVerticesVboSet().getAttributeIndex());
    }
    private void bindTexture(GlRawDataObj glRawDataObj){
        if(glRawDataObj.isUsingTexture()){
            bindVboArrayBuffer(getTextureVboId());
            bufferArrayData(glRawDataObj.getTextureArray(),2,getTextureVboSet().getAttributeIndex());
        }
    }
    private void bindNormals(GlRawDataObj glRawDataObj){
        if(glRawDataObj.isUsingNormals()){
            bindVboArrayBuffer(getNormalsVboId());
            bufferArrayData(glRawDataObj.getNormalsArray(),3,getNormalsVboSet().getAttributeIndex());
        }
    }
    private void bindIndices(GlRawDataObj glRawDataObj){
        bindVboElementBuffer(getIndicesVboId());
        bufferElementArrayData(glRawDataObj.getIndicesArray());
    }
    private void bindColors(GlRawDataObj glRawDataObj){
        if(!glRawDataObj.isUsingTexture()){
            bindVboArrayBuffer(getColorsVboId());
            bufferArrayData(glRawDataObj.getColorsArray(),3,getColorVboSet().getAttributeIndex());
        }

    }


    protected void activateAndBindBaseTexture(){
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,getTextureVboSet().getTextureId());
        if (hasTransparency()){
            disableCulling();
        }
    }
    protected void disableCulling(){
        GL11.glDisable(GL_CULL_FACE);
    }


    public void glModelActivate(){
        bindVertexArrayAndIndices();
        activateAndBindBaseTexture();
        enableVerticesAndTextureAttrib();
    }
    public void glModelDeactivate(){
        enableCulling();
        glDataAbstractEnablerInterface.disableVertex();
        unbindAll();
        glDisable(GL_TEXTURE_2D);
    }
    public void drawModel(){
        glDrawElements(GL_TRIANGLES, getIndicesVboSet().getSizeOfData(), GL_UNSIGNED_INT, 0);
    }
    public boolean hasTransparency() {
        return transparency;
    }
}

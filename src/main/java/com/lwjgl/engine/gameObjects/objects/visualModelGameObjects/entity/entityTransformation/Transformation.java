package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityTransformation;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.visualModel.AbstractedGameObject;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;

public abstract class Transformation extends AbstractedGameObject {

    public Transformation(){
        setProjectionMatrix(EngineComponentsSharedObject.getEngineComponentSharedObject().getActiveWindow().getWidth(),EngineComponentsSharedObject.getEngineComponentSharedObject().getActiveWindow().getHeight());

    }

    protected void transformEntityModel(){
        setTransformationMatrixIdentity();
        worldMatrix.translate(position);
        worldMatrix.rotateX(rotation.x);
        worldMatrix.rotateY(rotation.y);
        worldMatrix.rotateZ(rotation.z);
        worldMatrix.scale(scale);
    }

    public void moveDistance(float distance){
        position.x += (float) Math.sin(rotation.y) * distance;
        position.z += (float) Math.cos(rotation.y) * distance;
    }

}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainTypes.textureTerrainTypes.uniqueMultipleTexturesTerrainType;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.TerrainModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.models.UniqueVerticesTerrainIndices;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.models.UniquePerpendicularNormals;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainTextures.models.UniqueVerticesMultipleTerrainTextures;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.models.UniqueVerticesTerrain;
import com.lwjgl.engine.openGL.shaders.terrainShaders.textureTerrainShaders.TextureArrayTerrainShader;

public class UniqueMultipleTexturesTerrainType extends TerrainModel<TextureArrayTerrainShader> {


    public UniqueMultipleTexturesTerrainType(int glShaderIndex,float x,float y,float z,float terrainWidth,float terrainDepth){
        super(glShaderIndex,x,y,z,terrainWidth,terrainDepth);
    }


    public void generateTerrain(String mapFilename,String texturesFilename,String heighMapFilename,int rowSize,int columnSize,float maxHeight,String...colors){
        UniqueVerticesTerrain uniqueVerticesTerrain = new UniqueVerticesTerrain(heighMapFilename,rowSize,columnSize,x,z,y,terrainWidth,terrainDepth,maxHeight);
        UniqueVerticesTerrainIndices uniqueVerticesTerrainIndices = new UniqueVerticesTerrainIndices(rowSize * columnSize,6);
        UniqueVerticesMultipleTerrainTextures uniqueVerticesMultipleTerrainTextures = new UniqueVerticesMultipleTerrainTextures(rowSize * columnSize * 4,2);
        UniquePerpendicularNormals uniquePerpendicularNormals = new UniquePerpendicularNormals(rowSize * columnSize * 4);


        uniqueVerticesTerrain.generateVertices();
        uniqueVerticesTerrainIndices.generateUniqueIndices();
        uniqueVerticesMultipleTerrainTextures.generateTextures(mapFilename,rowSize,columnSize,colors);
        uniquePerpendicularNormals.generateNormals(rowSize ,columnSize);

        loadGlDataAbstract(texturesFilename,uniqueVerticesTerrain.getAllVertices(),uniqueVerticesMultipleTerrainTextures.getAllTextures(),uniquePerpendicularNormals.getAllNormals(),uniqueVerticesTerrainIndices.getAllIndices());

    }

    private void loadGlDataAbstract(String texturesFilename,float[] vertices,float[] textures,float[] normals,int[] indices){
        UniqueMultipleTexturesTerrainDataAbstract uniqueMultipleTexturesTerrainDataAbstract = new UniqueMultipleTexturesTerrainDataAbstract();
        uniqueMultipleTexturesTerrainDataAbstract.initObjAndTexture(vertices,normals,textures,indices,texturesFilename);
        uniqueMultipleTexturesTerrainDataAbstract.loadTextureData(texturesFilename);
        loadGlData(uniqueMultipleTexturesTerrainDataAbstract);
    }


    @Override
    public void update(TextureArrayTerrainShader glShaderAbstractProgram) {

    }
}

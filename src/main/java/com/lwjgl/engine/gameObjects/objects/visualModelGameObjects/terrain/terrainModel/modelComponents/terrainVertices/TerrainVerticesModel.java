package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices;


import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.ComponentModel;
import org.joml.Vector3f;

public abstract class TerrainVerticesModel extends ComponentModel<Float> {
    protected final int verticesSizeComponent = 3;
    protected final int rowVertices;
    protected final int columnVertices;
    protected final float xPosition;
    protected final float zPosition;
    protected final float yPosition;
    protected final float terrainWidth;
    protected final float terrainDepth;
    protected final float widthOfSquare;
    protected final float depthOfSquare;



    public TerrainVerticesModel(int rowVertices, int columnVertices, float x,float y,float z,float terrainWidth,float terrainDepth,float widthOfSquare,float depthOfSquare){
        super(new Float[rowVertices * columnVertices * 3],3);
        this.rowVertices = rowVertices;
        this.columnVertices = columnVertices;
        this.xPosition = x;
        this.yPosition = y;
        this.zPosition = z;
        this.terrainWidth = terrainWidth;
        this.terrainDepth = terrainDepth;
        this.widthOfSquare = widthOfSquare;
        this.depthOfSquare = depthOfSquare;

    }

    public void addVertice(float x,float y,float z){
        try {
            addNewValuesCurrentPointer(x,y,z);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addVertice(float[] vertices){
        try {
            addNewValuesCurrentPointer(vertices[0],vertices[1],vertices[2]);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void changeVerticeOnPointer(int pointer,float x,float y,float z){
        addNewValue(pointer,x);
        addNewValue(pointer + 1,y);
        addNewValue(pointer + 2,z);
    }

    public float[] getVertices(int row,int column){
        int baseIndex = ((row * columnVertices * 3) + (column * 3));
        return new float[]{
                getValue(baseIndex),
                getValue(baseIndex + 1),
                getValue(baseIndex + 2),
        };
    }
    public float[] getVertices(int baseIndex){
        return new float[]{
                getValue(baseIndex),
                getValue(baseIndex + 1),
                getValue(baseIndex + 2),
        };
    }
    public float[] getAllVertices(){
        float[] primitive = new float[realSizeOfArray()];
        int index = 0;
        for(float vertice : getArray()){
            primitive[index] = vertice;
            ++index;
        }
        return primitive;
    }

    public Vector3f getVertexComponent(int triangleIndex){
        float x = getValue(triangleIndex);
        float y = getValue(triangleIndex + 1);
        float z = getValue(triangleIndex + 2);
        return new Vector3f(x,y,z);
    }


    @Override
    public String toString() {
        int index = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   x    y    z\n\n");
        stringBuilder.append("( ");
        for(int i = 0; i < getArray().length; ++i){
            if(getArray()[i] != null){
                if (index % 3 == 0 && index != 0){
                    stringBuilder.append(")\n");
                    stringBuilder.append("( ");
                    stringBuilder.append(getArray()[i]);
                    stringBuilder.append("  ");
                }else{
                    stringBuilder.append(getArray()[i]);
                    stringBuilder.append("  ");
                }
                ++index;
            }
        }
        stringBuilder.append(") ");
        return stringBuilder.toString();
    }

}

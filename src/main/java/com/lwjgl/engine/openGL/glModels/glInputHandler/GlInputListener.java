package com.lwjgl.engine.openGL.glModels.glInputHandler;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;

public interface GlInputListener {
    default void listenForInputs(EntityModel entity, GlInputHandler glInputHandler){};
    default void listenForInputs(CameraModel camera, GlInputHandler glInputHandler){};
}

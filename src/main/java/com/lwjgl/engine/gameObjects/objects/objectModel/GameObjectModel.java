package com.lwjgl.engine.gameObjects.objects.objectModel;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public interface GameObjectModel {
    void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram);

}

package com.lwjgl.engine.openGL.glModels.glAbstractData.glStatementAssist;

import com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets.GlVboDataSet;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class GlStatementAssist extends GlVboDataSet {

    protected GlStatementAssist() {

    }

    //VAO STATEMENTS

    protected void generateNewVaoId(){
        vaoId = glGenVertexArrays();
        System.out.println("generating "+vaoId);
    }
    protected void bindVertexArray(){
        glBindVertexArray(vaoId);
    }

    //VBO STATEMENTS

    protected int generateNewVbo(){
        return glGenBuffers();
    }
    protected void bindVboArrayBuffer(int vbo){
        glBindBuffer(GL_ARRAY_BUFFER,vbo);
    }
    protected void bindVboElementBuffer(int vbo){
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo);
    }


    protected void bufferArrayData(FloatBuffer floatBuffer, int size, int attribIndex){
        glBufferData(GL_ARRAY_BUFFER,floatBuffer,GL_STATIC_DRAW);
        glVertexAttribPointer(attribIndex,size,GL_FLOAT,false,0,0);

    }
    protected void bufferArrayData(IntBuffer intBuffer, int size, int attribIndex){
        glBufferData(GL_ARRAY_BUFFER,intBuffer,GL_STATIC_DRAW);
        glVertexAttribPointer(attribIndex,size,GL_FLOAT,false,0,0);
    }

    protected void bufferElementArrayData(IntBuffer intBuffer){
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,intBuffer,GL_STATIC_DRAW);
    }
    protected void bufferElementArrayData(FloatBuffer floatBuffer){
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,floatBuffer,GL_STATIC_DRAW);
    }




    protected void unbindVboElementBuffer(){
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    }

    protected void unbindVboArrayBuffer(){
        glBindBuffer(GL_ARRAY_BUFFER,0);
    }
    protected void unbindVertexArray(){
        glBindVertexArray(0);
    }
}

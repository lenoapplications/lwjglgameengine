package com.lwjgl.engine.utils.math.mathConstruction.matrices;


public class Matrix4f {
    private final int SIZE = 4 * 4;
    private float[] matrix;

    public Matrix4f(){
        this.matrix = new float[SIZE];
        resetMatrix();
    }


    public float[] getMatrix() {
        return matrix;
    }

    public void setMatrix(float[] matrix) {
        this.matrix = matrix;
    }

    private void resetMatrix(){
        for (int i = 0 ; i < SIZE ; ++i){
            matrix[i] = 0;
        }
    }


    @Override
    public String toString() {
        int row = 0;
        System.out.println("{");
        for(int i = 0 ; i < SIZE; ++i){
            if (row == 4){
                row = 0;
                System.out.println("");
            }
            System.out.print(matrix[i]);
            System.out.print(" , ");
            ++row;
        }
        System.out.println("}");
        return "king";
    }

}

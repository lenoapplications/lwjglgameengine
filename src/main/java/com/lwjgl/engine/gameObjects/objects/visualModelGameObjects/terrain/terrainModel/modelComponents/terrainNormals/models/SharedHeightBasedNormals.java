package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.models.SharedVerticesTerrainIndices;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.TerrainNormalsModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.models.SharedVerticesTerrain;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SharedHeightBasedNormals extends TerrainNormalsModel {
    public SharedHeightBasedNormals(int size) {
        super(size);
    }

    public void generateNormals(SharedVerticesTerrain sharedVerticesTerrain, SharedVerticesTerrainIndices sharedVerticesTerrainIndices){
        Set<Integer> appliedNormals = new HashSet<>();

        for(int squareIndex = 0, indice = 0; squareIndex < sharedVerticesTerrainIndices.sizeOfArray(); ++squareIndex, indice+=6){
            int[] indices = sharedVerticesTerrainIndices.getIndices(squareIndex);
            Vector3f leftDownVertex = sharedVerticesTerrain.getVertexComponent(indices[0] * 3);
            Vector3f rightDownVertex = sharedVerticesTerrain.getVertexComponent(indices[1] * 3);
            Vector3f rightUpVertex = sharedVerticesTerrain.getVertexComponent(indices[2] * 3);
            Vector3f leftUpVertex = sharedVerticesTerrain.getVertexComponent(indices[4] * 3);

            Vector3f bottomTriangle = setupNormalForTriangle(leftDownVertex,rightDownVertex,rightUpVertex);
            Vector3f upperTriangle = setupNormalForTriangle(leftUpVertex,leftDownVertex,rightDownVertex);

            if(!appliedNormals.contains(indices[0])){
                addNormals(bottomTriangle.x,bottomTriangle.y,bottomTriangle.z);
                appliedNormals.add(indices[0]);
            }
            if(!appliedNormals.contains(indices[1])){
                addNormals(bottomTriangle.x,bottomTriangle.y,bottomTriangle.z);
                appliedNormals.add(indices[1]);
            }
            if(!appliedNormals.contains(indices[3])){
                addNormals(upperTriangle.x,upperTriangle.y,upperTriangle.z);
                appliedNormals.add(indices[3]);
            }

            if(!appliedNormals.contains(indices[4])){
                addNormals(upperTriangle.x,upperTriangle.y,upperTriangle.z);
                appliedNormals.add(indices[4]);
            }

        }
    }

    private Vector3f setupNormalForTriangle(Vector3f vertexOne,Vector3f vertexTwo, Vector3f vertexThree){
        Vector3f tangent1 = vertexTwo.sub(vertexOne);
        Vector3f tangent2 = vertexThree.sub(vertexOne);
        return tangent2.cross(tangent1).normalize();
    }
}

package com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.glUniformVariables.GlUniformVariables;
import com.lwjgl.engine.utils.resourceReader.stringReader.StringReader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL32;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

public abstract class GlShaderAbstractProgram extends GlUniformVariables {
    private boolean programActive;

    public GlShaderAbstractProgram(){
        this.programActive = false;
    }

    public abstract void init();

    public void activateProgram() {
        if (!programActive){
            glUseProgram(shaderProgramId);
            programActive = true;
        }
    }

    public void deactivateProgram() {
        glUseProgram(-1);
        this.programActive = false;
    }


    protected void cleanUp(){
        glUseProgram(shaderProgramId);
        glDetachShader(shaderProgramId,vertexShaderId);
        if(geometryShaderId != -1){
            glDetachShader(shaderProgramId,geometryShaderId);
        }
        glDetachShader(shaderProgramId,fragmentShaderId);
        glDeleteShader(vertexShaderId);
        glDeleteShader(fragmentShaderId);
        glDeleteProgram(shaderProgramId);
    }

    protected void loadVertexShader(String... filename){
        vertexShaderId = loadShaderResource(GL_VERTEX_SHADER,filename);
    }
    protected void loadFragmentShader(String... filename){
        fragmentShaderId = loadShaderResource(GL_FRAGMENT_SHADER,filename);
    }
    protected void loadGeometryShader(String...filename){
        geometryShaderId = loadShaderResource(GL_GEOMETRY_SHADER,filename);
    }
    protected void createShaderProgram(){
        shaderProgramId = glCreateProgram();
        glAttachShader(shaderProgramId,vertexShaderId);
        if(geometryShaderId != -1){
            glAttachShader(shaderProgramId,geometryShaderId);
        }
        glAttachShader(shaderProgramId,fragmentShaderId);
        glLinkProgram(shaderProgramId);
        glValidateProgram(shaderProgramId);
        setupBasicUniformVariables();
        setupCustomUniformVariables();

    }


    private int loadShaderResource(int type,String...filePath){
        StringBuilder stringBuilder = StringReader.readFromResource(filePath);

        int shaderId = glCreateShader(type);
        glShaderSource(shaderId,stringBuilder);
        glCompileShader(shaderId);

        if(glGetShaderi(shaderId, GL_COMPILE_STATUS )== GL11.GL_FALSE){
            System.out.println(glGetShaderInfoLog(shaderId, 500));
            System.err.println("Could not compile shader!");
            System.exit(-1);
        }
        return shaderId;
    }

    public void setProgramActive(boolean active){
        this.programActive = active;
    }
    public boolean isProgramActive() {
        return programActive;
    }
}

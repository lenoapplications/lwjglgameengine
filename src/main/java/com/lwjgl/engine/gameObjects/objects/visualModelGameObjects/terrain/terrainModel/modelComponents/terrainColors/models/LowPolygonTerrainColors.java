package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.TerrainColorsModel;

import java.util.List;

public class LowPolygonTerrainColors extends TerrainColorsModel {


    public LowPolygonTerrainColors(int rowVertices, int columnVertices) {
        super(rowVertices, columnVertices);
    }


    public void generateColors(List<String> colorMap){
        for(String color : colorMap){
            int clr = Integer.parseInt(color);
            float  red   =( (clr & 0x00ff0000) >> 16 ) / 255f;
            float  green = ((clr & 0x0000ff00) >> 8) / 255f;
            float  blue  =  (clr & 0x000000ff) / 255f;
            double random = 0.00009 + Math.random() * (0.1 - 0.00009);

            red +=random;
            green +=random;
            blue +=random;
            addColor(red,green,blue);
            addColor(red,green,blue);
            addColor(red,green,blue);
            addColor(red,green,blue);
        }
    }

}

package com.lwjgl.engine.utils.math.mathFunctions;

import com.lwjgl.engine.utils.math.mathConstruction.matrices.Matrix4f;

import static com.lwjgl.engine.utils.math.mathFunctions.TrigFunctions.getCos;
import static com.lwjgl.engine.utils.math.mathFunctions.TrigFunctions.getSin;

public class Matrix4Func {
    private static final int row1Column1 =0+0*4;
    private static final int row1Column2 =1+0*4;
    private static final int row1Column3 =2+0*4;
    private static final int row1Column4 =3+0*4;
    private static final int row2Column1 =0+1*4;
    private static final int row2Column2 =1+1*4;
    private static final int row2Column3 =2+1*4;
    private static final int row2Column4 =3+1*4;
    private static final int row3Column1 =0+2*4;
    private static final int row3Column2 =1+2*4;
    private static final int row3Column3 =2+2*4;
    private static final int row3Column4 =3+2*4;
    private static final int row4Column1 =0+3*4;
    private static final int row4Column2 =1+3*4;
    private static final int row4Column3 =2+3*4;
    private static final int row4Column4 =3+3*4;

    public static void setIdentityMatrix(Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[row1Column1] = 1;
        matrix[row2Column2] = 1;
        matrix[row3Column3] = 1;
        matrix[row4Column4] = 1;

        matrix[row1Column2] = 0;
        matrix[row1Column3] = 0;
        matrix[row1Column4] = 0;
        matrix[row2Column1] = 0;
        matrix[row2Column3] = 0;
        matrix[row2Column4] = 0;
        matrix[row3Column1] = 0;
        matrix[row3Column2] = 0;
        matrix[row3Column4] = 0;
        matrix[row4Column1] = 0;
        matrix[row4Column2] = 0;
        matrix[row4Column3] = 0;

    }

    public static void setTranslationMatrix(float x,float y,float z, Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[row1Column4] = x;
        matrix[row2Column4] = y;
        matrix[row3Column4] = z;
    }

    public static void setXRotationMatrix(float angle, Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[row2Column2] = getCos(angle);
        matrix[row2Column3] =(-1) * getSin(angle);
        matrix[row3Column2] = getSin(angle);
        matrix[row3Column3] = getCos(angle);
    }
    public static void setYRotationMatrix(float angle,Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[0 + 0 * 4] = getCos(angle);
        matrix[2 + 0 * 4] =getSin(angle);
        matrix[0 + 2 * 4] =(-1) * getSin(angle);
        matrix[2 + 2 * 4] = getCos(angle);
    }

    public static void setZRotationMatrix(float angle,Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[0 + 0 * 4] = getCos(angle);
        matrix[1 + 0 * 4] = (-1) * getSin(angle);
        matrix[0 + 1 * 4] = getSin(angle);
        matrix[1 + 1 * 4] = getCos(angle);
    }

    public static void setProjectionMatrix(float yScale,float xScale,float farPlusNear,float farTimeNear,Matrix4f matrix4f){
        float[] matrix = matrix4f.getMatrix();
        matrix[row1Column1] = xScale;
        matrix[row2Column2] = yScale;
        matrix[row3Column3] = farPlusNear;
        matrix[row3Column4] = farTimeNear;

    }


    public static void multiplyMatrices4f(Matrix4f m1,Matrix4f m2){
        float[] newMatrix = new float[4*4];
        float[] m1Matrix = m1.getMatrix();
        float[] m2Matrix = m2.getMatrix();
        multiplyOperation(newMatrix,m1Matrix,m2Matrix);

        m1.setMatrix(newMatrix);
    }

    public static void multiplyMatrices4f(Matrix4f m1,Matrix4f m2, Matrix4f newM3){
        float[] newMatrix = newM3.getMatrix();
        float[] m1Matrix = m1.getMatrix();
        float[] m2Matrix = m2.getMatrix();
        multiplyOperation(newMatrix,m1Matrix,m2Matrix);
    }


    private static void multiplyOperation(float[] newMatrix, float[] m1, float[] m2){
        multiplyRowWithColumns(newMatrix,m1,m2,0);
        multiplyRowWithColumns(newMatrix,m1,m2,1);
        multiplyRowWithColumns(newMatrix,m1,m2,2);
        multiplyRowWithColumns(newMatrix,m1,m2,3);
    }


    private static void multiplyRowWithColumns(float[] newMatrix,float[] m1,float[] m2,int newMatrixRow){
        newMatrix[0 + newMatrixRow * 4] = calculateDotProduct(m1,m2,newMatrixRow,0);
        newMatrix[1 + newMatrixRow * 4] = calculateDotProduct(m1,m2,newMatrixRow,1);
        newMatrix[2 + newMatrixRow * 4] = calculateDotProduct(m1,m2,newMatrixRow,2);
        newMatrix[3 + newMatrixRow * 4] = calculateDotProduct(m1,m2,newMatrixRow,3);
    }


    private static float calculateDotProduct(float[] m1,float[] m2,int m1Row,int m2Column){
        final float a = m1[0 + m1Row * 4] * m2[m2Column + 0 * 4];
        final float b = m1[1 + m1Row * 4] * m2[m2Column + 1 * 4];
        final float c = m1[2 + m1Row * 4] * m2[m2Column + 2 * 4];
        final float d = m1[3 + m1Row * 4] * m2[m2Column + 3 * 4];

        return a+b+c+d;
    }

}

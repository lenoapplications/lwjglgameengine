package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals.TerrainNormalsModel;


public class SharedPerpendicularNormals extends TerrainNormalsModel {
    public SharedPerpendicularNormals(int size) {
        super(size);
    }


    public void generateNormals(int rowVertices,int columnVertices){
        System.out.println("size   "+realSizeOfArray());
        int size = 0;
        for(int row = 0 ; row < rowVertices; ++row){
            for(int column = 0; column < columnVertices; ++column){
                addNormals(0,1,0f);
                ++size;
            }
        }
        System.out.println("size is  "+size);
        System.out.println("pointer is  "+pointer);
    }
}

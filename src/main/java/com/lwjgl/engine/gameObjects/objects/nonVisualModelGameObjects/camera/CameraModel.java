package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera;

import com.lwjgl.engine.gameObjects.objects.objectModel.GameObjectModel;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public abstract class CameraModel implements GameObjectModel {
    protected float pitch;
    protected float yaw;
    protected float roll;

    protected final Matrix4f viewMatrix;
    protected final Vector3f position;

    protected final GlInputHandler glInputHandler;
    protected final GlInputListener glInputListener;

    public CameraModel(GlInputListener glInputListener){
        this.position = new Vector3f(0f,2f,-5f);
        this.viewMatrix = new Matrix4f();
        this.pitch = 0;
        this.yaw = 0f;
        this.roll = 0f;

        viewMatrix.identity();
        glInputHandler = EngineComponentsSharedObject.getEngineComponentSharedObject().getGlInputHandler();
        this.glInputListener = glInputListener;
    }


    public void changeXPosition(float x){
        position.x = x;
    }
    public void changeYPosition(float y){
        position.y = y;
    }
    public void changeZPosition(float z){
        position.z = z;
    }
    public void increaseXPosition(float x){
        position.x += x;
    }
    public void increaseYPosition(float y){
        position.y +=y;
    }
    public void increaseZPosition(float z){
        position.z +=z;
    }


    public void increaseYaw(float yaw){
        this.yaw +=yaw;
    }
    public void changeYaw(float yaw){
        this.yaw = yaw;
    }
    public void increasePitch(float pitch){
        this.pitch += pitch;
    }
    public void changePitch(float pitch){
        this.pitch = pitch;
    }

    public void setViewMatrixToIdentity() {
        viewMatrix.identity();
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public abstract void resetCameraToDefault();


    protected void transformViewMatrix(){
        glInputListener.listenForInputs(this,glInputHandler);
        viewMatrix.identity();
        viewMatrix.rotate((float) (pitch), new Vector3f(1, 0, 0));
        viewMatrix.rotate((float) (yaw), new Vector3f(0, 1, 0));

        Vector3f negativeCameraPos = new Vector3f(-position.x,-position.y,-position.z);
        viewMatrix.translate(negativeCameraPos);

    }

}

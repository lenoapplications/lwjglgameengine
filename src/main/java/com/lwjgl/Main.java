package com.lwjgl;

import com.lwjgl.engine.engineLoop.loopModels.FrameRunner;
import com.lwjgl.engine.engineStarter.EngineStarter;
import com.lwjgl.engine.gameObjects.holder.GameObjectHolder;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.models.LowPolygonTerrainColors;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainTypes.colorTerrainTypes.lowColorPolygonTerrainType.LowColorPolygonTerrainType;
import com.lwjgl.engine.openGL.shaders.terrainShaders.colorTerrainShaders.LowColorPolygonTerrainShader;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import com.lwjgl.engine.utils.resourceReader.pixelReader.PixelReader;


public class Main implements FrameRunner {
    private static EntityModel model;


    public static void main(String[] args) throws InterruptedException {


        EngineStarter engineStarter = new EngineStarter();

        engineStarter.setGlShadersControllerSize(2);
        engineStarter.initEngine(640,480,"Test");

        int index2 = EngineComponentsSharedObject.getEngineComponentSharedObject().getGlShaderController().addNewGlShaderAbstractProgram(new LowColorPolygonTerrainShader());
        GameObjectHolder gameObjectHolder = EngineComponentsSharedObject.getEngineComponentSharedObject().getGameObjectHolder();

//        UniqueMultipleTexturesTerrainType uniqueMultipleTexturesTerrainType = new UniqueMultipleTexturesTerrainType(index2,1f,0,-0.1f,100f,200f);
//        uniqueMultipleTexturesTerrainType.generateTerrain("./res/assets/roadTerrain.txt","grass2.png","./res/assets/roadTerrainHeight.txt",20,20,2f,"#017e20","#b8a308");
//
//        gameObjectHolder.getTerrainObjectHolder().addNewTerrain(uniqueMultipleTexturesTerrainType);


        LowColorPolygonTerrainType lowColorPolygonTerrainType = new LowColorPolygonTerrainType(index2,0.05f,0.0f,0,10,12);
        lowColorPolygonTerrainType.generateTerrain("./res/assets/colorRgbTerrain.txt","./res/assets/roadTerrainHeight.txt",50,50,2);

        gameObjectHolder.getTerrainObjectHolder().addNewTerrain(lowColorPolygonTerrainType);


        engineStarter.startEngine();
    }


    @Override
    public void runFrame() {

    }
}

package com.lwjgl.engine.openGL.glModels.glInputHandler;

import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import static org.lwjgl.glfw.GLFW.*;


public class GlInputHandler {
    private final GlInputHandlerStates glInputHandlerStates;
    private final GLFWKeyCallback glfwKeyCallback;
    private final GLFWCursorPosCallback glfwCursorPosCallback;
    private final GLFWMouseButtonCallback glfwMouseButtonCallback;
    private final GLFWScrollCallback glfwScrollCallback;

    private final int KEYBOARD_SIZE = 512;
    private final int MOUSE_BUTTONS_SIZE = 16;
    private final int MOUSE_SCROLL_SIZE = 2;

    private final boolean[] activeKeys = new boolean[KEYBOARD_SIZE];
    private final int[] keyStates = new int[KEYBOARD_SIZE];

    private final long[] mouseButtonTimes = new long[MOUSE_BUTTONS_SIZE];
    private final int[] mouseButtonStates = new int[MOUSE_BUTTONS_SIZE];

    private final double[] scrollXYOffsets = new double[MOUSE_SCROLL_SIZE];
    private final double[] mouseXYPosition = new double[2];
    private final double[] mouseXYDifference = new double[2];



    private class MouseMoveCallback extends GLFWCursorPosCallback{

        @Override
        public void invoke(long window, double xpos, double ypos) {
            mouseXYDifference[0] = xpos - mouseXYPosition[0];
            mouseXYDifference[1] = ypos - mouseXYPosition[1];

            mouseXYPosition[0] = xpos;
            mouseXYPosition[1] = ypos;
        }
    }

    private class MouseButtonCallback extends GLFWMouseButtonCallback{
        private long MOUSE_BUTTON_PRESS_TIME = 0;

        @Override
        public void invoke(long window, int button, int action, int mods) {
            mouseButton1Event(button,action);
            mouseButtonStates[button] = action;

        }

        private void mouseButton1Event(int button,int action){
            switch (action){
                case GLFW_RELEASE:{
                    mouseButtonTimes[button] = System.currentTimeMillis() - MOUSE_BUTTON_PRESS_TIME;
                    MOUSE_BUTTON_PRESS_TIME = 0;
                    return;
                }
                case GLFW_PRESS:{
                    MOUSE_BUTTON_PRESS_TIME = System.currentTimeMillis();
                    mouseButtonTimes[button] = MOUSE_BUTTON_PRESS_TIME;
                    return;
                }
            }
        }
    }
    private class MouseScrollCallback extends GLFWScrollCallback{

        @Override
        public void invoke(long window, double xoffset, double yoffset) {
            scrollXYOffsets[0] = xoffset;
            scrollXYOffsets[1] = yoffset;
        }

    }
    private class KeyboardCallback extends GLFWKeyCallback{

        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            activeKeys[key] = action != GLFW_RELEASE;
            keyStates[key] = action;
        }

    }

    public GlInputHandler(){
        this.glInputHandlerStates = new GlInputHandlerStates();
        this.glfwKeyCallback = new KeyboardCallback();
        this.glfwMouseButtonCallback = new MouseButtonCallback();
        this.glfwCursorPosCallback = new MouseMoveCallback();
        this.glfwScrollCallback = new MouseScrollCallback();
        EngineComponentsSharedObject.getEngineComponentSharedObject().setGlInputHandler(this);
    }


    public boolean isKeyClicked(int key){
        if(keyStates[key] == GLFW_PRESS && !glInputHandlerStates.KEY_PRESSED){
            glInputHandlerStates.KEY_PRESSED = true;
            return false;
        }else if (keyStates[key] == GLFW_RELEASE && glInputHandlerStates.KEY_PRESSED){
            glInputHandlerStates.KEY_PRESSED = false;
            System.out.println("key clicked");
            return true;
        }else{
            return false;
        }
    }
    public boolean isKeyPressed(int key){
        if(keyStates[key] == GLFW_REPEAT || keyStates[key] == GLFW_PRESS){
            if (glInputHandlerStates.KEY_PRESSED){
                glInputHandlerStates.KEY_PRESSED = false;
            }
            return true;
        }
        return false;
    }
    public boolean isMouseButtonClicked(int button){
        if(mouseButtonTimes[button] != 0 && mouseButtonTimes[button] != -1){
            if (mouseButtonTimes[button] < 200){
                System.out.println("Click");
                mouseButtonTimes[button] = 0;
                return true;
            }
        }
        return false;
    }
    public boolean isMouseButtonPressed(int button){
        return mouseButtonStates[button] == GLFW_PRESS && ((System.currentTimeMillis() - mouseButtonTimes[button] > 200));
    }


    public double getXOffset(){
        double xOffset = mouseXYDifference[0];
        mouseXYDifference[0] = 0.0f;
        return xOffset;
    }
    public double getYOffset(){
        double yOffset = mouseXYDifference[1];
        mouseXYDifference[1] = 0.0f;
        return yOffset;
    }

    public float getScrollXOffset(){
        float xOffset = (float) scrollXYOffsets[0];
        scrollXYOffsets[0] = 0.0;
        return xOffset;
    }
    public float getScrollYOffset(){
        float yOffset = (float) scrollXYOffsets[1];
        scrollXYOffsets[1] = 0.0;
        return yOffset;
    }



    public GLFWKeyCallback getGlfwKeyCallback() {
        return glfwKeyCallback;
    }
    public GLFWCursorPosCallback getGlfwCursorPosCallback(){
        return glfwCursorPosCallback;
    }
    public GLFWMouseButtonCallback getGlfwMouseButtonCallback() {
        return glfwMouseButtonCallback;
    }
    public GLFWScrollCallback getGlfwScrollCallback() {
        return glfwScrollCallback;
    }

}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.ComponentModel;

public abstract class TerrainColorsModel extends ComponentModel<Float> {

    public TerrainColorsModel(int rowVertices,int columnVertices) {
        super(new Float[rowVertices * columnVertices * 3], 3);
    }


    public void addColor(float r,float g,float b){
        try {
            addNewValuesCurrentPointer(r,g,b);
        }catch (Exception e){

        }
    }


    public float[] getAllColors(){
        float[] primitive = new float[realSizeOfArray()];
        int index = 0;
        for(float color : getArray()){
            primitive[index] = color;
            ++index;
        }
        return primitive;
    }



    @Override
    public String toString() {
        int index = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   x    y    z\n\n");
        stringBuilder.append("( ");
        for(int i = 0; i < getArray().length; ++i){
            if(getArray()[i] != null){
                if (index % 3 == 0 && index != 0){
                    stringBuilder.append(")\n");
                    stringBuilder.append("( ");
                    stringBuilder.append(getArray()[i]);
                    stringBuilder.append("  ");
                }else{
                    stringBuilder.append(getArray()[i]);
                    stringBuilder.append("  ");
                }
                ++index;
            }
        }
        stringBuilder.append(") ");
        return stringBuilder.toString();
    }
}

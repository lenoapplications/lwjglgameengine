package com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.glShaderIdentHolder;

public class GlShaderIdentHolder {
    protected int shaderProgramId;
    protected int vertexShaderId;
    protected int fragmentShaderId;
    protected int geometryShaderId;


    public GlShaderIdentHolder(){
        this.shaderProgramId = -1;
        this.vertexShaderId = -1;
        this.geometryShaderId = -1;
        this.fragmentShaderId = -1;
    }

    public int getGeometryShaderId() {
        return geometryShaderId;
    }


    public int getShaderProgramId() {
        return shaderProgramId;
    }

    public int getVertexShaderId() {
        return vertexShaderId;
    }

    public int getFragmentShaderId() {
        return fragmentShaderId;
    }
}

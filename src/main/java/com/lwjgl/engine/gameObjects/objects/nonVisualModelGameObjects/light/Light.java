package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light;

import com.lwjgl.engine.gameObjects.objects.objectModel.GameObjectModel;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import org.joml.Vector3f;


public class Light implements GameObjectModel {
    private int indexId;
    private Vector3f position;
    private Vector3f color;


    public Light(int indexId){
        this.position = new Vector3f(0,1,0);
        this.color = new Vector3f(1f,1f,1f);
        this.indexId = indexId;
    }

    public Vector3f getColor() {
        return color;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setIndexId(int indexId) {
        this.indexId = indexId;
    }

    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.BASIC_LIGHTS[indexId],position);
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.BASIC_LIGHTS_COLORS[indexId],color);

    }
}

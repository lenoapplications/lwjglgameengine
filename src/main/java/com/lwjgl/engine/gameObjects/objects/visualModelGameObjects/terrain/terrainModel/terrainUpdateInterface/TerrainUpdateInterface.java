package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.terrainUpdateInterface;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public interface TerrainUpdateInterface<A extends GlShaderAbstractProgram> {
    void update(A glShaderAbstractProgram);
}

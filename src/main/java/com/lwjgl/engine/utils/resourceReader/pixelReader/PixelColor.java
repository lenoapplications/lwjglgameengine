package com.lwjgl.engine.utils.resourceReader.pixelReader;

public class PixelColor {
    public int red;
    public int blue;
    public int green;

    public PixelColor(int red, int green, int blue) {
        this.red = red;
        this.blue = blue;
        this.green = green;
    }
}

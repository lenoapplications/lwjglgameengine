package com.lwjgl.engine.sharedObject.engineComponentsSharedObject;

import com.lwjgl.engine.engineLoop.EngineLoop;
import com.lwjgl.engine.gameObjects.holder.GameObjectHolder;
import com.lwjgl.engine.gui.window.Window;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderController.GlShaderController;

public class EngineComponentsSharedObject {
    private static final EngineComponentsSharedObject engineComponentSharedObject = new EngineComponentsSharedObject();

    private Window activeWindow;
    private EngineLoop activeEngineLoop;
    private GlShaderController glShaderController;
    private GameObjectHolder gameObjectHolder;
    private GlInputHandler glInputHandler;

    private EngineComponentsSharedObject(){
    }


    public void setGlInputHandler(GlInputHandler glInputHandler) {
        this.glInputHandler = glInputHandler;
    }

    public GlInputHandler getGlInputHandler() {
        return glInputHandler;
    }

    public GameObjectHolder getGameObjectHolder() {
        return gameObjectHolder;
    }

    public void setGameObjectHolder(GameObjectHolder gameObjectHolder) {
        this.gameObjectHolder = gameObjectHolder;
    }


    public void setGlShaderController(GlShaderController glShaderController) {
        this.glShaderController = glShaderController;
    }

    public GlShaderController getGlShaderController() {
        return glShaderController;
    }

    public void setActiveWindow(Window activeWindow) {
        this.activeWindow = activeWindow;
    }
    public Window getActiveWindow() {
        return activeWindow;
    }

    public EngineLoop getActiveEngineLoop() {
        return activeEngineLoop;
    }
    public void setActiveEngineLoop(EngineLoop activeEngineLoop) {
        this.activeEngineLoop = activeEngineLoop;
    }



    public static EngineComponentsSharedObject getEngineComponentSharedObject(){
        return engineComponentSharedObject;
    }

}

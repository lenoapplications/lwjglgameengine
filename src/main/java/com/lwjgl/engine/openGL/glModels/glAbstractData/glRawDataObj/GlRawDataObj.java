package com.lwjgl.engine.openGL.glModels.glAbstractData.glRawDataObj;

import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;
import com.lwjgl.engine.openGL.glModels.glAbstractData.glDataAbstractEnabler.GlDataAbstractEnablerInterface;
import com.lwjgl.engine.utils.bufferUtils.BufferUtils;
import com.lwjgl.engine.utils.resourceReader.stringReader.StringReader;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;

public class GlRawDataObj {
    private int[] indicesArray;
    private float[] textureArray;
    private float[] normalsArray;
    private float[] verticesArray;
    private float[] colorsArray;
    private final boolean isUsingTexture;
    private final boolean isUsingNormals;

    public GlRawDataObj(String filename){
        init(filename);
        this.isUsingNormals = true;
        this.isUsingTexture = true;
        this.colorsArray = null;
    }
    public GlRawDataObj(boolean useTexture,boolean isUsingNormals){
        this.isUsingTexture = useTexture;
        this.isUsingNormals = isUsingNormals;
    }

    public void setColorsArray(float[] colorsArray) {
        this.colorsArray = colorsArray;
    }

    public void setIndicesArray(int[] indicesArray) {
        this.indicesArray = indicesArray;
    }

    public void setNormalsArray(float[] normalsArray) {
        this.normalsArray = normalsArray;
    }

    public void setTextureArray(float[] textureArray) {
        this.textureArray = textureArray;
    }

    public void setVerticesArray(float[] verticesArray) {
        this.verticesArray = verticesArray;
    }

    private void init(String filename){
        List<Vector3f> vertices  = new ArrayList<>();
        List<Vector2f> textures  = new ArrayList<>();
        List<Vector3f> normals  = new ArrayList<>();
        List<Integer> indices  = new ArrayList<>();

        String[] data = StringReader.readFromOther(filename).toString().split("\n");
        int index = 0;

        for (String line : data){
            if (line.startsWith("v ")){
                Vector3f vertex = createVector3f(line.split("\\s"));
                vertices.add(vertex);
            }else if(line.startsWith("vt ")){
                Vector2f texture = createVector2f(line.split("\\s"));
                textures.add(texture);
            }else if(line.startsWith("vn ")){
                Vector3f normal = createVector3f(line.split("\\s"));
                normals.add(normal);
            }else if(line.startsWith("f ")){
                textureArray = new float[vertices.size() * 2];
                normalsArray = new float[vertices.size() * 3];

                secondPartOfParsingObjFile(index,data,indices,textures,normals);
                break;
            }
            ++index;
        }
        verticesArray = new float[vertices.size() * 3];
        indicesArray = new int[indices.size()];

        int vertexPointer = 0;
        for(Vector3f vertex : vertices){
            verticesArray[vertexPointer++] = vertex.x;
            verticesArray[vertexPointer++] = vertex.y;
            verticesArray[vertexPointer++] = vertex.z;
        }

        for(int i = 0; i < indices.size(); i++){
            indicesArray[i] = indices.get(i);
        }

    }

    private void secondPartOfParsingObjFile(int startIndex,String[] data,List<Integer> indices,List<Vector2f> textures,List<Vector3f> normals){
        String line;
        for (;startIndex < data.length ; ++startIndex){
            line = data[startIndex];

            if (line.startsWith("f ")){
                String[] currentLine = line.split("\\s");
                String[] vertex1 = currentLine[1].split("/");
                String[] vertex2 = currentLine[2].split("/");
                String[] vertex3 = currentLine[3].split("/");

                processVertex(vertex1,indices,textures,normals);
                processVertex(vertex2,indices,textures,normals);
                processVertex(vertex3,indices,textures,normals);
            }
        }
    }

    private void processVertex(String[] vertexData,List<Integer> indices,List<Vector2f> textures,List<Vector3f> normals){
        int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
        indices.add(currentVertexPointer);
        Vector2f currentTex = textures.get(Integer.parseInt(vertexData[1])-1);
        textureArray[currentVertexPointer * 2] = currentTex.x;
        textureArray[currentVertexPointer * 2 + 1] = 1 - currentTex.y;

        Vector3f currentNorm = normals.get(Integer.parseInt(vertexData[2])-1);

        normalsArray[currentVertexPointer * 3] = currentNorm.x;
        normalsArray[currentVertexPointer * 3 + 1] = currentNorm.y;
        normalsArray[currentVertexPointer * 3 + 2] = currentNorm.z;
    }



    private Vector3f createVector3f(String[] chars){
        float x = Float.parseFloat(chars[1].replaceAll("[/ A-Za-z]",""));
        float y = Float.parseFloat(chars[2].replaceAll("[/ A-Za-z]",""));
        float z = Float.parseFloat(chars[3].replaceAll("[/ A-Za-z]",""));
        return new Vector3f(x,y,z);
    }

    private Vector2f createVector2f(String[] chars){
        float x = Float.parseFloat(chars[1].replaceAll("[/ A-Za-z]",""));
        float y = Float.parseFloat(chars[2].replaceAll("[/ A-Za-z]",""));
        return new Vector2f(x,y);
    }

    private FloatBuffer convertToFloatBuffer(float[] floats){
        return BufferUtils.createFloatBuffer(floats);
    }
    private IntBuffer convertToIntBuffer(int[] integers){
        return BufferUtils.createIntBuffer(integers);
    }



    public IntBuffer getIndicesArray() {
        return convertToIntBuffer(indicesArray);
    }

    public FloatBuffer getTextureArray() {
        return convertToFloatBuffer(textureArray);
    }

    public FloatBuffer getNormalsArray() {
        return convertToFloatBuffer(normalsArray);
    }

    public FloatBuffer getVerticesArray() {
        return convertToFloatBuffer(verticesArray);
    }

    public FloatBuffer getColorsArray(){
        return convertToFloatBuffer(colorsArray);
    }


    public int indicesSize(){
        return indicesArray.length;
    }
    public int verticesSize(){
        return verticesArray.length;
    }
    public int normalsSize(){
        return normalsArray.length;
    }
    public int textureSize(){
        return textureArray.length;
    }
    public int colorsSize(){
        return colorsArray.length;
    }

    public boolean isUsingTexture() {
        return isUsingTexture;
    }

    public boolean isUsingNormals() {
        return isUsingNormals;
    }

    public GlDataAbstractEnablerInterface createEnablerInterface(GlDataAbstract glDataAbstract){
        int size = 2;
        if(isUsingNormals){
            size++;
        }
        int[] attributeIndexes = new int[size];
        attributeIndexes[0] = glDataAbstract.getVerticesVboSet().getAttributeIndex();
        attributeIndexes[1] = (isUsingTexture)?glDataAbstract.getTextureVboSet().getAttributeIndex() : glDataAbstract.getColorVboSet().getAttributeIndex();

        if(isUsingNormals){
            attributeIndexes[2] = glDataAbstract.getNormalsVboSet().getAttributeIndex();
        }

        return new GlDataAbstractEnablerInterface(attributeIndexes) {
            @Override
            public void enableVertex() {
                for(int i : attributeIndexes){
                    glEnableVertexAttribArray(i);
                }
            }

            @Override
            public void disableVertex() {
                for(int i : attributeIndexes){
                    glDisableVertexAttribArray(i);
                }
            }
        };
    }
}

package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel;

import com.lwjgl.engine.gameObjects.objects.objectModel.GameObjectModel;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import org.joml.Vector3f;

public abstract class LightModel implements GameObjectModel {
    protected final Vector3f lightPosition;
    protected final Vector3f lightColor;

    public LightModel(Vector3f lightPosition,Vector3f lightColor){
        this.lightPosition = lightPosition;
        this.lightColor = lightColor;
    }

    public Vector3f getLightColor() {
        return lightColor;
    }

    public Vector3f getLightPosition() {
        return lightPosition;
    }
}

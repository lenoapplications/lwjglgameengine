package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainTextures;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.ComponentModel;

public abstract class TerrainTexturesModel extends ComponentModel<Float> {

    public TerrainTexturesModel(int size) {
        super(new Float[size * 2], 2);

    }

    public void addTexture(float x,float y){
        try {
            addNewValuesCurrentPointer(x,y);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void changeTextureOnPointer(int pointer,float x,float y){
        addNewValue(pointer,x);
        addNewValue(pointer + 1,y);
    }
    public float[] getTexture(int index){
        return new float[]{
                getValue(index),
                getValue(index + 1),
        };
    }
    public float[] getAllTextures(){
        float[] primitive = new float[realSizeOfArray()];
        int index = 0;
        for(float texture : getArray()){
            primitive[index] = texture;
            ++index;
        }
        return primitive;
    }


    @Override
    public String toString() {
        int index = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Texture\n\n");
        stringBuilder.append("( ");
        for(float v : getArray()){
            if (index % skipSize == 0 && index != 0){
                stringBuilder.append(")\n");
                stringBuilder.append("( ");
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }else{
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }
            ++index;
        }
        stringBuilder.append(") ");
        return stringBuilder.toString();
    }

}

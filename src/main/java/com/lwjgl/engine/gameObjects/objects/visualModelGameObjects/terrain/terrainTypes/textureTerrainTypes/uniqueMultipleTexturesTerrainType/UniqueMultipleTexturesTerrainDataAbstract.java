package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainTypes.textureTerrainTypes.uniqueMultipleTexturesTerrainType;

import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;
import org.lwjgl.opengl.GL13;

import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.glActiveTexture;


public class UniqueMultipleTexturesTerrainDataAbstract extends GlDataAbstract {


    public UniqueMultipleTexturesTerrainDataAbstract() {
        super(true);
    }

    public void loadTextureData(String filename){
        getTextureVboSet().setupTextureImage(filename,2);
    }


    @Override
    protected void activateAndBindBaseTexture() {
        glActiveTexture(GL13.GL_TEXTURE0);
        glBindTexture(GL13.GL_TEXTURE_2D,getTextureVboSet().getTextureId());
        if (hasTransparency()){
            disableCulling();
        }
    }
}

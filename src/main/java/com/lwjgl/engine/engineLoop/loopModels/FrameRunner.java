package com.lwjgl.engine.engineLoop.loopModels;

public interface FrameRunner {
    void runFrame();
}

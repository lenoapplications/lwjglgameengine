package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainNormals;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.ComponentModel;

public abstract class TerrainNormalsModel extends ComponentModel<Float> {

    public TerrainNormalsModel(int size) {
        super(new Float[size * 3], 3);
    }


    public void addNormals(float x,float y,float z){
        try {
            addNewValuesCurrentPointer(x,y,z);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void changeNormalsOnPointer(int pointer,float x,float y,float z){
        addNewValue(pointer,x);
        addNewValue(pointer + 1,y);
        addNewValue(pointer + 2, z);
    }
    public float[] getNormals(int index){
        return new float[]{
                getValue(index),
                getValue(index + 1),
                getValue(index + 2),
        };
    }
    public float[] getAllNormals(){
        float[] primitive = new float[realSizeOfArray()];
        int index = 0;
        for(float normals : getArray()){
            primitive[index] = normals;
            ++index;
        }
        return primitive;
    }

    @Override
    public String toString() {
        int index = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Indices\n\n");
        System.out.println(getArray().length);
        stringBuilder.append("( ");
        for(float v : getArray()){
            if (index % skipSize == 0 && index != 0){
                stringBuilder.append(")\n");
                stringBuilder.append("( ");
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }else{
                stringBuilder.append(v);
                stringBuilder.append("  ");
            }
            ++index;
        }
        stringBuilder.append(") ");
        return stringBuilder.toString();
    }
}

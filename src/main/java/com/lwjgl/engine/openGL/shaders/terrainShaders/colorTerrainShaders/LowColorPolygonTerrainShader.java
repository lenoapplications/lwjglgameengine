package com.lwjgl.engine.openGL.shaders.terrainShaders.colorTerrainShaders;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public class LowColorPolygonTerrainShader extends GlShaderAbstractProgram {
    public final String SUN_SOURCE = "sunPosition";
    public final String SUN_COLOR = "sunColor";

    @Override
    public void init() {
        loadVertexShader("terrainShader","colorTerrainShaders","lowPolygonTerrainShader","gls","LowPolyTerrainShader.vert");
        loadFragmentShader("terrainShader","colorTerrainShaders","lowPolygonTerrainShader","gls","LowPolyTerrainShader.frag");
        loadGeometryShader("terrainShader","colorTerrainShaders","lowPolygonTerrainShader","gls","LowPolyTerrainShader.glsl");
        createShaderProgram();
    }

    @Override
    protected void setupCustomUniformVariables() {
        setNewUniformLocation(SUN_SOURCE);
        setNewUniformLocation(SUN_COLOR);
    }
}

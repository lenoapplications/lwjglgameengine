package com.lwjgl.engine.engineLoop;
//class FrameManager {
//    private:
//    int desiredFramePerSec;
//    float frameStartTime;
//    float currentFrameTime;
//    float timePerFrame;
//
//    int frameCounter;
//    int numberOfFramesInSecond;
//    float oneSecondTimer;
//
//
//    public:
//    FrameManager(int desiredFramePerSecond);
//    ~FrameManager();
//
//    void changeDesiredFramePerSec(int desiredFramePerSec);
//    void mesaureFrameStartTime();
//    void mesaureFramePassTime();
//    void checkCurrentFrameTime();
//
//
//    bool checkIfOneSecondPass();
//    int getFrameCountInSecond();
//
//
//};
//
//
//FrameManager::FrameManager(int desiredFramePerSecond):desiredFramePerSec(desiredFramePerSecond) {
//        this->timePerFrame = 1000 / desiredFramePerSec;
//        this->oneSecondTimer = SDL_GetTicks();
//        }
//
//        FrameManager::~FrameManager() {
//        std::cout<<"FrameManager deleted"<<std::endl;
//        }
//
//        void FrameManager::changeDesiredFramePerSec(int desiredFramePerSec) {
//        this->desiredFramePerSec = desiredFramePerSec;
//        this->timePerFrame = 1000 / this->desiredFramePerSec;
//
//        }
//
//        void FrameManager::mesaureFrameStartTime() {
//        frameStartTime = SDL_GetTicks();
//        }
//
//        void FrameManager::mesaureFramePassTime() {
//        currentFrameTime = SDL_GetTicks() - frameStartTime;
//
//        }
//
//        void FrameManager::checkCurrentFrameTime() {
//        if (timePerFrame > currentFrameTime){
//        SDL_Delay(timePerFrame - currentFrameTime);
//        }
//        ++frameCounter;
//
//        }
//
//        int FrameManager::getFrameCountInSecond() {
//        return numberOfFramesInSecond;
//        }
//
//        bool FrameManager::checkIfOneSecondPass() {
//        if ((SDL_GetTicks() - oneSecondTimer) >= 1000){
//        numberOfFramesInSecond = frameCounter;
//        frameCounter = 0;
//        oneSecondTimer = SDL_GetTicks();
//        return true;
//        }
//        return false;
//
//        }

public class FrameController {

    private int desiredFramePerSec;
    private long frameStartTime;
    private long currentFrameTime;
    private long timePerFrame;
    private long oneSecondTimer;

    private int frameCounter = 0;

    public FrameController(int desiredFramePerSec){
        this.desiredFramePerSec = desiredFramePerSec;
        this.timePerFrame = 1000 / desiredFramePerSec;
        oneSecondTimer = 0;
    }

    public void nowTime(){
        this.frameStartTime = System.currentTimeMillis();
    }
    public void passedTime(){
        this.currentFrameTime = System.currentTimeMillis() - frameStartTime;
    }
    public void checkTimePerFrame(){
        if (timePerFrame > currentFrameTime){
            try {
                Thread.sleep((long) (timePerFrame-currentFrameTime));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        ++frameCounter;
    }
    public void checkIfOneSecondPass(){
        if ((System.currentTimeMillis() - oneSecondTimer) >= 1000) {
            System.out.println("Frame passed "+frameCounter);
            frameCounter = 0;
            oneSecondTimer = System.currentTimeMillis();
        }
    }


    public static void main(String[] args){
        FrameController frameController = new FrameController(60);
        while(true){
            frameController.nowTime();
            frameController.passedTime();
            frameController.checkTimePerFrame();
            frameController.checkIfOneSecondPass();
        }
    }

}

#version 410 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform vec3 sunColor;
uniform mat4 worldMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 sunSource;
uniform vec3 lightPositions[4];
uniform vec3 lightColors[4];
uniform vec3 lightAttenuation[4];


in vec3 toCameraVector[];
in vec3 triangleMainColor[];

out vec3 finalColor;


vec3 calcTriangleNormal(){
    vec3 tangent1 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec3 tangent2 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
    vec3 normal = cross(tangent2, tangent1);
    return normalize(normal);
}

float calculateBrightness(vec3 normal,vec4 worldPosition){
    vec3 unitLightPosition = normalize(sunSource - worldPosition.xyz);
    float dotProduct = dot(unitLightPosition,normal);
    return max(dotProduct,0.3);
}

float calculateAttuneationFactor(int index,vec4 worldPosition){
    float distance = length(lightPositions[index] - worldPosition.xyz);
    float attenuationFactor = 2;
    return 1.0/(lightAttenuation[index].x + lightAttenuation[index].y *distance + lightAttenuation[index].z  * distance * distance);
}

vec3 calculateColor(vec4 worldPosition,float brightness){
    vec3 totalColor = vec3(0,0,0);
    for(int light = 0; light < 4 ; ++light){
        totalColor += triangleMainColor[0] * ( (brightness * lightColors[light] ) * calculateAttuneationFactor(light,worldPosition) );
    }
    return totalColor;
}


void main(void){
    vec3 normal = calcTriangleNormal();
    vec4 baseOfTriangle = worldMatrix * gl_in[0].gl_Position;
    float brightness = calculateBrightness(normal,baseOfTriangle);

    for(int i = 0; i < 3 ; ++i){
        vec4 worldPosition = worldMatrix * gl_in[i].gl_Position;
        gl_Position = projectionMatrix * viewMatrix * worldPosition;
        finalColor = calculateColor(worldPosition,brightness);

        EmitVertex();
    }
    EndPrimitive();
}
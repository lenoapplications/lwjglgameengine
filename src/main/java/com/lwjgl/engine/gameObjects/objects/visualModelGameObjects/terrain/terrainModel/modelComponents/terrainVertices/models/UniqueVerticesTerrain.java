package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.TerrainVerticesModel;
import com.lwjgl.engine.utils.resourceReader.pixelReader.PixelReader;

import java.util.List;

public class UniqueVerticesTerrain extends TerrainVerticesModel {
    private final float MAX_PIXEL_COLOR = (256 * 256 * 256);
    private final int rowSize;
    private final int columnSize;
    private final float maxHeight;
    private final List<String> heightMap;


    public UniqueVerticesTerrain(String heightFilename,int rowSize, int columnSize, float xPosition, float zPosition, float yPosition, float terrainWidth, float terrainDepth,float maxHeight) {
        super(2*rowSize, 2*columnSize,xPosition,yPosition,zPosition,terrainWidth,terrainDepth,terrainWidth/(float)columnSize,terrainWidth/(float)rowSize);
        this.rowSize = rowSize;
        this.columnSize = columnSize;
        this.maxHeight = maxHeight;
        this.heightMap = PixelReader.readImageColorsFormTxtFile(heightFilename);

    }
    public void generateVertices(){
        for(int row = 0; row < rowSize; ++row){
            if(row == 0){
                firstRowGeneration(row);
            }else{
                otherRowGeneration(row);
            }
        }
    }


    private void firstRowGeneration(int row) {
        for (int column = 0; column < columnSize; ++column) {
            if (column == 0){
                firstRowFirstColumnGeneration(row,column);
            }else{
                firstRowOtherColumnGeneration(row,column);
            }
        }
    }
    private void firstRowFirstColumnGeneration(int row, int column){
        float startXPosition = xPosition + (column * widthOfSquare);
        float startZPosition = zPosition + (row * depthOfSquare);
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

        startXPosition += widthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

        startXPosition += -widthOfSquare;
        startZPosition += depthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

        startXPosition +=widthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

    }

    private void firstRowOtherColumnGeneration(int row, int column){
        float startXPosition = xPosition + (column * widthOfSquare);
        float startZPosition = zPosition + (row * depthOfSquare);

        float[] rightDownPointOfPreviousSquare = getVertices(getPointer() - (3 * verticesSizeComponent) );
        addVertice(rightDownPointOfPreviousSquare);

        startXPosition +=widthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

        float[] rightUpPointOfPreviousSquare = getVertices(getPointer() - (3 * verticesSizeComponent) );
        addVertice(rightUpPointOfPreviousSquare);

        startZPosition += depthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

    }

    private void otherRowGeneration(int row){
        for (int column = 0; column < columnSize; ++column) {
            if (column == 0){
                otherRowFirstColumnGeneration(row,column);
            }else{
                otherRowOtherColumnGeneration(row,column);
            }
        }
    }

    private void otherRowFirstColumnGeneration(int row,int column){
        float startXPosition = xPosition + (column * widthOfSquare);
        float startZPosition = zPosition + (row * depthOfSquare);

        float[] leftPointBeneath = getVertices(((getPointer()/3)-(columnSize * 4 - 2)) * 3);
        addVertice(leftPointBeneath);

        float[] rightPointBeneath = getVertices(((getPointer()/3)-(columnSize * 4 - 2)) * 3);
        addVertice(rightPointBeneath);

        startZPosition += depthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);

        startXPosition += widthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);
    }

    private void otherRowOtherColumnGeneration(int row,int column){
        float startXPosition = xPosition + (column * widthOfSquare);
        float startZPosition = zPosition + (row * depthOfSquare);

        float[] rightDownPointOfPreviousSquare = getVertices(getPointer() - (3 * verticesSizeComponent) );
        addVertice(rightDownPointOfPreviousSquare);

        float[] rightPointBeneath = getVertices(((getPointer()/3)-(columnSize * 4 - 2)) * 3);
        addVertice(rightPointBeneath);

        float[] rightUpPointOfPreviousSquare = getVertices(getPointer() - (3 * verticesSizeComponent) );
        addVertice(rightUpPointOfPreviousSquare);

        startXPosition += widthOfSquare;
        startZPosition += depthOfSquare;
        addVertice(startXPosition,calculateHeight(row,column),startZPosition);
    }


    private float calculateHeight(int row,int column){
       return 0.0f;
    }

}

package com.lwjgl.engine.utils.resourceReader.stringReader;


import java.io.*;

public class StringReader {

    public static StringBuilder readFromOther(String file){
        StringBuilder resourceBuilder = new StringBuilder();
        readFromReader(resourceBuilder,file);
        return resourceBuilder;
    }
    public static StringBuilder readFromResource(String...path){
        StringBuilder pathBuild = new StringBuilder("/shaders");
        for(int i = 0; i < path.length ; ++i){
            pathBuild.append("/");
            pathBuild.append(path[i]);
        }
        return readFromResourceReader(pathBuild.toString());
    }

    private static StringBuilder readFromResourceReader(String path){
        try {
            BufferedInputStream reader = new BufferedInputStream(StringReader.class.getResourceAsStream(path));
            byte[] bytes = new byte[reader.available()];
            reader.read(bytes);
            return new StringBuilder(new String(bytes));
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    private static void readFromReader(StringBuilder stringBuilder,String file){
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine())!=null){
                stringBuilder.append(line).append("//\n");
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}

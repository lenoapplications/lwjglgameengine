#version 410 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 worldMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 sunSource;

out vec3 toCameraVector;
out vec3 triangleMainColor;


void main(){
    vec4 worldPosition = worldMatrix * vec4(position,1.0f);

    gl_Position = vec4(position,1.0f);
    triangleMainColor = color;
    toCameraVector = (inverse(viewMatrix) * vec4(0.0f,0.0f,0.0f,1.0f)).xyz - worldPosition.xyz;
}
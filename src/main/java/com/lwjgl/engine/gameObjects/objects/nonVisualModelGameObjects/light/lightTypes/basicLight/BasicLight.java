package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightTypes.basicLight;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel.models.PointLight;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import org.joml.Vector3f;

public class BasicLight extends PointLight {
    private int indexId;

    public BasicLight(Vector3f lightPosition, Vector3f lightColor, Vector3f attenuation) {
        super(lightPosition, lightColor, attenuation);
        this.indexId = 0;
    }

    public void setIndexId(int indexId) {
        this.indexId = indexId;
    }

    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.BASIC_LIGHTS[indexId],lightPosition);
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.BASIC_LIGHTS_COLORS[indexId],lightColor);
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.BASIC_LIGHTS_ATTENUATION[indexId],attenuation);

    }
}

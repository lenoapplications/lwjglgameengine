package com.lwjgl.engine.utils.math.mathFunctions;


import com.lwjgl.engine.utils.math.mathConstruction.matrices.Matrix4f;
import com.lwjgl.engine.utils.math.mathConstruction.vectors.Vector3d;

public class Vector3dFunc {

    private Vector3dFunc(){
    }


    public static void translateVector3d(Vector3d vector3d,Matrix4f matrix,float x,float y,float z){
//        float[] translationMatrix = matrix.makeTranslationMatrix(x,y,z);
//        multiplyVectorByMatrix(vector3d,translationMatrix);
       // matrix.makeIdentityMatrix();
    }
    public static void scaleVector3d(Vector3d vector3d,Matrix4f matrix){
//        float[] scaledMatrix = matrix.makeScaledMatrix(2);
//        multiplyVectorByMatrix(vector3d,scaledMatrix);
        //matrix.makeIdentityMatrix();
    }
    public static void rotateVector3dX(Vector3d vector3d,Matrix4f matrix,float angle){
//        float[] xRotationMatrix = matrix.makeXRotationMatrix(angle);
        //multiplyVectorByMatrix(vector3d,xRotationMatrix);
        //matrix.makeIdentityMatrix();

    }
    public static void rotateVector3dY(Vector3d vector3d,Matrix4f matrix,float angle){
        //float[] yRotationMatrix = matrix.makeYRotationMatrix(angle);
        //multiplyVectorByMatrix(vector3d,yRotationMatrix);
        //matrix.makeIdentityMatrix();

    }
    public static void rotateVector3dZ(Vector3d vector3d,float angle,Matrix4f matrix){
        //float[] zRotationMatrix = matrix.makeZRotationMatrix(angle);
        //multiplyVectorByMatrix(vector3d,zRotationMatrix);
       // matrix.makeIdentityMatrix();

    }

    private static void multiplyVectorByMatrix(Vector3d vector3d,float[] matrix){
        float x = getDotProduct(vector3d,matrix,0);
        float y = getDotProduct(vector3d,matrix,1);
        float z = getDotProduct(vector3d,matrix,2);
        float w = getDotProduct(vector3d,matrix,3);
        setNewValuesToVector3d(x,y,z,w,vector3d);

    }

    private static float getDotProduct(Vector3d vector3d,float[] matrix,int matrixRow){
        float a = vector3d.getX() * matrix[0 + matrixRow * 4];
        float b = vector3d.getY() * matrix[1 + matrixRow * 4];
        float c = vector3d.getZ() * matrix[2 + matrixRow * 4];
        float d = vector3d.getW() * matrix[3 + matrixRow * 4];

        return a+b+c+d;
    }

    private static void setNewValuesToVector3d(float x, float y,float z,float w,Vector3d vector3d){
        vector3d.setX(x);
        vector3d.setY(y);
        vector3d.setZ(z);
        vector3d.setW(w);
    }


}

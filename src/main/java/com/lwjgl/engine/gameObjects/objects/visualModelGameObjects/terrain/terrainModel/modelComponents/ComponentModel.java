package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents;

import org.joml.Vector3f;

public abstract class ComponentModel<A> {
    private final A[] array;
    protected int skipSize;
    protected int pointer;

    public ComponentModel(A[] array , int skipSize){
        this.pointer = 0;
        this.array = array;
        this.skipSize = skipSize;
    }

    protected void addNewValue(int index,A value){
        this.array[index] = value;
    }
    protected void addNewValueToCurrentPointer(A value){
        this.array[pointer] = value;
        ++this.pointer;
    }
    protected void addNewValuesCurrentPointer(A...values) throws Exception {
        if(values.length != skipSize){
                throw new Exception("To much indexes, it requires "+skipSize);
        }
        for(A value : values){
            this.array[pointer] = value;
            ++this.pointer;
        }
    }

    public A getValue(int index){
        return this.array[index];
    }

    public A[] getArray() {
        return array;
    }
    public void pointTo(int pointer){
        this.pointer = pointer;
    }
    public int sizeOfArray(){
        return array.length / skipSize;
    }
    public int realSizeOfArray(){
        return array.length;
    }
    public void resetPointer(){
        pointer = 0;
    }
    public int getPointer(){
        return pointer;
    }

    public int getSkipSize() {
        return skipSize;
    }
}

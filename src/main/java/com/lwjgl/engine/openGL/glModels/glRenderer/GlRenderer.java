package com.lwjgl.engine.openGL.glModels.glRenderer;

import com.lwjgl.engine.gameObjects.holder.GameObjectHolder;
import com.lwjgl.engine.gameObjects.holder.LightsObjectHolder;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightTypes.basicLight.BasicLight;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.EntityGroup;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.Light;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.TerrainModel;
import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;
import com.lwjgl.engine.openGL.glModels.glModelsHolder.GlModelsHolder;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderController.GlShaderController;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import java.util.HashMap;
import java.util.List;


public class GlRenderer {
    private final GlShaderController glShaderController;
    private final GlModelsHolder glModelsHolder;
    private final CameraModel camera;
    private final BasicLight[] basicLights;
    private final HashMap<Integer, EntityGroup> entities;
    private final List<TerrainModel> terrains;

    public GlRenderer(GameObjectHolder gameObjectHolder){
        this.glShaderController = EngineComponentsSharedObject.getEngineComponentSharedObject().getGlShaderController();
        this.glModelsHolder = gameObjectHolder.getEntitiesObjectHolder().getGlModelsHolder();
        this.camera = gameObjectHolder.getCamera();
        this.basicLights = gameObjectHolder.getLightsObjectHolder().getBasicLightModels();
        this.entities = gameObjectHolder.getEntitiesObjectHolder().getEntities();
        this.terrains = gameObjectHolder.getTerrainObjectHolder().getTerrainModelList();
    }

    public void objectsRender(){
        renderEntitesLightsCamera();
        renderTerrains();
    }

    private void renderEntitesLightsCamera(){
        GlDataAbstract model;
        EntityGroup entityGroup;

        for(Integer key : entities.keySet()){
            model = glModelsHolder.getModel(key);
            entityGroup = entities.get(key);

            if (glShaderController.activateGlShaderAbstractProgram(entityGroup.getIndexGlShaderAbstractProgram())){
                glShaderController.loadUniformsToCurrentShader(camera);
                glShaderController.loadUniformsToCurrentShader(basicLights[0]);
                glShaderController.loadUniformsToCurrentShader(basicLights[1]);
                glShaderController.loadUniformsToCurrentShader(basicLights[2]);
                glShaderController.loadUniformsToCurrentShader(basicLights[3]);
            }
            model.glModelActivate();

            for(EntityModel entity : entityGroup.getEntityModelList()){
                glShaderController.loadUniformsToCurrentShader(entity);
                model.drawModel();
            }
        }
        glShaderController.deactivateGlShaderAbstractProgramLastUsed();
    }
    private void renderTerrains(){
        GlDataAbstract model;

        for (TerrainModel terrain : terrains){
            if (glShaderController.activateGlShaderAbstractProgram(terrain.getIndexGlShaderAbstractProgram())){
                glShaderController.loadUniformsToCurrentShader(camera);
                glShaderController.loadUniformsToCurrentShader(basicLights[0]);
                glShaderController.loadUniformsToCurrentShader(basicLights[1]);
                glShaderController.loadUniformsToCurrentShader(basicLights[2]);
                glShaderController.loadUniformsToCurrentShader(basicLights[3]);
            }
            model = terrain.getGlDataAbstract();
            model.glModelActivate();
            glShaderController.loadUniformsToCurrentShader(terrain);
            model.drawModel();
            model.glModelDeactivate();
        }
        glShaderController.deactivateGlShaderAbstractProgramLastUsed();
    }


}

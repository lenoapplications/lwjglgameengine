package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.camerTypes;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.visualModel.AbstractedGameObject;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public class ThirdPersonCamera extends CameraModel {
    private final AbstractedGameObject abstractedGameObject;
    private float distanceFromPlayer;
    private float angleAroundPlayer;
    private float sensitivity;


    public ThirdPersonCamera(float angleAroundPlayer,float distanceFromPlayer,AbstractedGameObject abstractedGameObject,GlInputListener glInputListener) {
        super(glInputListener);
        this.abstractedGameObject = abstractedGameObject;
        this.distanceFromPlayer = -distanceFromPlayer;
        this.angleAroundPlayer = angleAroundPlayer;
        this.sensitivity = 1f;
    }

    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        float theta = (abstractedGameObject.getRotation().y + angleAroundPlayer);
        float horizontalDistance = calculateHorizontalDistance();
        float offsetX = (float) (horizontalDistance * Math.sin((theta)));
        float offsetZ = (float) (horizontalDistance * Math.cos((theta)));

        position.x = abstractedGameObject.getPosition().x + offsetX;
        position.z = abstractedGameObject.getPosition().z + offsetZ;
        position.y = abstractedGameObject.getPosition().y + calculateVerticalDistance() + 0.2f;

        yaw = -(abstractedGameObject.getRotation().y + angleAroundPlayer);

        transformViewMatrix();
        glShaderAbstractProgram.loadMatrix4f(glShaderAbstractProgram.VIEW_MATRIX,viewMatrix);
    }
    @Override
    public void increasePitch(float pitchIncrease) {
        pitchIncrease = (float) (sensitivity * (pitchIncrease));
        this.pitch -=pitchIncrease;
    }
    @Override
    public void resetCameraToDefault() {
        this.angleAroundPlayer = 0f;
        this.pitch = 0f;
    }

    public void setSensitivity(float sensitivity) {
        this.sensitivity = sensitivity;
    }
    public void increaseAngleAroundPlayer(float angleIncrease){
        angleIncrease = sensitivity * angleIncrease;
        this.angleAroundPlayer -= angleIncrease;
    }
    public void changeAngleAroundPlayer(float angle){
        this.angleAroundPlayer = angle;
    }
    public void increaseDistanceFromPlayer(float distanceIncrease){
        distanceIncrease = sensitivity * distanceIncrease;
        this.distanceFromPlayer -= distanceIncrease;
    }
    public void changeDistanceFromPlayer(float distance){
        this.distanceFromPlayer = distance;
    }



    private float calculateHorizontalDistance(){
        return (distanceFromPlayer * (float)Math.cos((pitch)));
    }
    private float calculateVerticalDistance(){
        return (distanceFromPlayer * (float)Math.sin((pitch)));
    }
}

package com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets;

import com.lwjgl.engine.openGL.glModels.glAbstractData.glDataIdentHolder.GlDataIdentHolder;
import com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets.set.*;

import static org.lwjgl.opengl.GL20.*;


public class GlVboDataSet extends GlDataIdentHolder {
    private VerticesVboSet verticesVboSet;
    private TextureVboSet textureVboSet;
    private IndicesVboSet indicesVboSet;
    private NormalsVboSet normalsVboSet;
    private ColorVboSet colorVboSet;

    protected GlVboDataSet(){

    }

    protected void setVertices(int verticesSize){
        setVerticesVboId(glGenBuffers());
        verticesVboSet = new VerticesVboSet(verticesSize);
    }
    protected void setTexture(int textureSize,String filename){
        setTextureVboId(glGenBuffers());
        textureVboSet = new TextureVboSet(textureSize);
        if (filename != null){
            textureVboSet.setupTextureImg(glGenTextures(),filename);
        }
    }
    protected void setTexture(){
        textureVboSet = new TextureVboSet(0);
    }
    protected void setIndices(int sizeOfIndices){
        setIndicesVboId(glGenBuffers());
        indicesVboSet = new IndicesVboSet(sizeOfIndices);
    }
    protected void setNormals(int sizeOfNormals){
        setNormalsVboId(glGenBuffers());
        normalsVboSet = new NormalsVboSet(sizeOfNormals);
    }
    protected void setColors(int sizeOfColors){
        setColorsVboId(glGenBuffers());
        colorVboSet = new ColorVboSet(sizeOfColors);
    }
    protected void setColors(){
        colorVboSet = new ColorVboSet(0);
    }

    public VerticesVboSet getVerticesVboSet() {
        return verticesVboSet;
    }

    public TextureVboSet getTextureVboSet() {
        return textureVboSet;
    }

    public IndicesVboSet getIndicesVboSet() {
        return indicesVboSet;
    }

    public NormalsVboSet getNormalsVboSet() {
        return normalsVboSet;
    }

    public ColorVboSet getColorVboSet() {
        return colorVboSet;
    }
}

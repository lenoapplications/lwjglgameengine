package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.TerrainIndicesModel;

public class UniqueVerticesTerrainIndices extends TerrainIndicesModel {

    public UniqueVerticesTerrainIndices(int size, int skipSize) {
        super(size,skipSize);
    }

    public void generateUniqueIndices(){
        int numberOfSquares = sizeOfArray();
        for(int i = 0; i < numberOfSquares ; i++){
            int baseIndex = i * 4;

            int leftDown = baseIndex;
            int rightDown = baseIndex + 1;
            int rightUp = baseIndex + 3;
            int leftUp = baseIndex + 2;

            addIndices(leftDown,rightDown,rightUp,rightUp,leftUp,leftDown);
        }
    }

}

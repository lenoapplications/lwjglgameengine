package com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets.set;



public abstract class GlVboDataAbstract {
    protected final int attributeIndex;
    protected final int sizeOfData;


    public GlVboDataAbstract(int attributeIndex,int sizeOfData){
        this.attributeIndex = attributeIndex;
        this.sizeOfData = sizeOfData;
    }

    public int getSizeOfData(){
        return sizeOfData;
    }
    public int getAttributeIndex(){
        return attributeIndex;
    }
}

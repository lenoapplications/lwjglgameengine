package com.lwjgl.engine.gameObjects.holder;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.EntityGroup;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;
import com.lwjgl.engine.openGL.glModels.glModelsHolder.GlModelsHolder;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

import java.util.HashMap;

public class EntitiesObjectHolder {
    private final HashMap<Integer, EntityGroup> entities;
    private final GlModelsHolder glModelsHolder;

    public EntitiesObjectHolder() {
        entities = new HashMap<>();
        glModelsHolder = new GlModelsHolder();

    }

    public int createNewGroupOfEntities(int indexGlShaderAbstractProgram,String objFilename,String textureFilename,boolean transparency){
        int groupKey = glModelsHolder.createNewModel(objFilename,textureFilename,transparency);

        if (groupKey != -1){
            EntityGroup entityGroup = new EntityGroup(indexGlShaderAbstractProgram);
            entities.put(groupKey,entityGroup);
        }
        return groupKey;
    }

    public int createNewGroupOfEntities(int indexGlShaderAbstractProgram,String objFilename,String textureFilename){
        int groupKey = glModelsHolder.createNewModel(objFilename,textureFilename,false);

        if (groupKey != -1){
            EntityGroup entityGroup = new EntityGroup(indexGlShaderAbstractProgram);
            entities.put(groupKey,entityGroup);
        }
        return groupKey;
    }

    public void addEntity(int group, EntityModel entityModel){
        if (entities.containsKey(group)){
            entities.get(group).getEntityModelList().add(entityModel);
            entityModel.activate();
        }
    }

    public GlModelsHolder getGlModelsHolder() {
        return glModelsHolder;
    }


    public HashMap<Integer, EntityGroup> getEntities() {
        return entities;
    }


}

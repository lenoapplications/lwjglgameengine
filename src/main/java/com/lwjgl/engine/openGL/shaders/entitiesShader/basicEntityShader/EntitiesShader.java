package com.lwjgl.engine.openGL.shaders.entitiesShader.basicEntityShader;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public class EntitiesShader extends GlShaderAbstractProgram {
    public final String LIGHT_POSITION_VECTOR = "lightPosition";
    public final String LIGHT_COLOR_VECTOR = "lightColor";

    @Override
    public void init() {
        loadVertexShader("entitiesShader","gls","BasicShader.vert");
        loadFragmentShader("entitiesShader","gls","BasicShader.frag");
        createShaderProgram();

    }

    @Override
    protected void setupCustomUniformVariables() {
        setNewUniformLocation(LIGHT_POSITION_VECTOR);
        setNewUniformLocation(LIGHT_COLOR_VECTOR);
    }
}

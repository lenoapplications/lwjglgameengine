#version 410 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoords;
layout (location = 2) in vec3 normal;

int MAX_LIGHT_AFFECT_OBJECT = 4;

uniform mat4 worldMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPositions[4];


out vec2 pass_textureCoords;
out vec3 surfaceNormal;
out vec3 lightDifferenceVectors[4];
out vec3 toCameraVector;



void calculateLightDifferenceVectors(vec4 worldPosition){
    for(int light = 0; light < MAX_LIGHT_AFFECT_OBJECT ; ++light ){
        lightDifferenceVectors[light] = lightPositions[light] - worldPosition.xyz;
    }
}

void main(){
    vec4 worldPosition = worldMatrix * vec4(position,1.0f);
    gl_Position = projectionMatrix * viewMatrix * worldPosition;
    pass_textureCoords = textureCoords;

    calculateLightDifferenceVectors(worldPosition);

    surfaceNormal = (worldMatrix * vec4(normal,0.0f)).xyz;
    toCameraVector = (inverse(viewMatrix) * vec4(0.0f,0.0f,0.0f,1.0f)).xyz - worldPosition.xyz;
}
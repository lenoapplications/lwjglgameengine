package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.camerTypes;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public class Camera extends CameraModel {


    public Camera(GlInputListener glInputListener) {
        super(glInputListener);
    }

    @Override
    public void resetCameraToDefault() {

    }

    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        transformViewMatrix();
        glShaderAbstractProgram.loadMatrix4f(glShaderAbstractProgram.VIEW_MATRIX,viewMatrix);
    }
}

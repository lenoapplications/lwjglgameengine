package com.lwjgl.engine.openGL.shaders.terrainShaders.textureTerrainShaders;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public class TextureTerrain3RgbShader extends GlShaderAbstractProgram {
    public final String BACKGROUND_TEXTURE = "backgroundTexture";
    public final String R_TEXTURE = "rTexture";
    public final String G_TEXTURE = "gTexture";
    public final String B_TEXTURE = "bTexture";
    public final String BLEND_MAP = "blendMap";

    @Override
    public void init() {
        loadVertexShader("terrainShader","textureTerrainShaders","gls","Rgb3TerrainShader.vert");
        loadFragmentShader("terrainShader","textureTerrainShaders","gls","Rgb3TerrainShader.frag");
        createShaderProgram();

    }

    @Override
    protected void setupCustomUniformVariables() {
        setNewUniformLocation(BACKGROUND_TEXTURE);
        setNewUniformLocation(R_TEXTURE);
        setNewUniformLocation(G_TEXTURE);
        setNewUniformLocation(B_TEXTURE);
        setNewUniformLocation(BLEND_MAP);
    }
}

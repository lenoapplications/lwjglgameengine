#version 410 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoords;
layout (location = 2) in vec3 normal;

uniform mat4 worldMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;


out vec2 pass_textureCoords;
out vec3 surfaceNormal;
out vec3 toLightSource;
out vec3 toCameraVector;

void main(){
    vec4 worldPosition = worldMatrix * vec4(position,1.0f);
    gl_Position = projectionMatrix * viewMatrix * worldPosition;
    pass_textureCoords = textureCoords;

    vec3 actualNormal = vec3(0,1,0);
    surfaceNormal = (worldMatrix * vec4(actualNormal,0.0f)).xyz;
    toLightSource = lightPosition - worldPosition.xyz;
    toCameraVector = (inverse(viewMatrix) * vec4(0.0f,0.0f,0.0f,1.0f)).xyz - worldPosition.xyz;
}
package com.lwjgl.engine.openGL.glModels.glShaders.glShaderController;

import com.lwjgl.engine.gameObjects.objects.objectModel.GameObjectModel;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;

public class GlShaderController {
    private final GlShaderAbstractProgram[] allShaders;
    private GlShaderAbstractProgram activeGlShaderAbstractProgram;
    private int currentActiveShaderProgram;


    public GlShaderController(int size){
        this.allShaders = new GlShaderAbstractProgram[size];
        EngineComponentsSharedObject.getEngineComponentSharedObject().setGlShaderController(this);
        this.currentActiveShaderProgram = -1;
    }


    public int addNewGlShaderAbstractProgram(GlShaderAbstractProgram glShaderAbstractProgram){
        glShaderAbstractProgram.init();
        int index = getFirstAvailableIndex();
        this.allShaders[index] = glShaderAbstractProgram;
        return index;
    }

    public GlShaderAbstractProgram glShaderAbstractProgram(int index) {
        return allShaders[index];
    }
    public boolean activateGlShaderAbstractProgram(int index){
        if (currentActiveShaderProgram != index){
            allShaders[index].activateProgram();
            activeGlShaderAbstractProgram = allShaders[index];
            currentActiveShaderProgram = index;
            return true;
        }
        return false;
    }
    public void deactivateGlShaderAbstractProgram(int index){
        allShaders[index].deactivateProgram();
    }
    public void deactivateGlShaderAbstractProgramLastUsed(){
        if(currentActiveShaderProgram != -1){
            allShaders[currentActiveShaderProgram].deactivateProgram();
            currentActiveShaderProgram = -1;
        }
    }

    public void loadUniformsToCurrentShader(GameObjectModel abstractedGameObject){
        abstractedGameObject.transformAndLoadUniforms(activeGlShaderAbstractProgram);
    }


    public GlShaderAbstractProgram getGlShaderAbstractProgram(int index) {
        return allShaders[index];
    }


    private int getFirstAvailableIndex(){
        for (int i = 0; i < allShaders.length ; i++){
            if (allShaders[i] == null){
                return i;
            }
        }
        return -1;
    }
}

package com.lwjgl.engine.utils.math.mathFunctions;

public class TrigFunctions {

    public static float getCos(float angle){
        double cosine = Math.cos(Math.toRadians(angle));
        return (float) cosine;
    }
    public static float getSin(float angle){
        double sinus = Math.sin(Math.toRadians(angle));
        return (float) sinus;
    }

}

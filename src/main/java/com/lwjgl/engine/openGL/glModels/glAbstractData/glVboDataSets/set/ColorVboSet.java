package com.lwjgl.engine.openGL.glModels.glAbstractData.glVboDataSets.set;

public class ColorVboSet extends GlVboDataAbstract {
    public ColorVboSet(int sizeOfData) {
        super(1, sizeOfData);
    }
}

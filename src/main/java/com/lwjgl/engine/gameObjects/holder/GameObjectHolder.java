package com.lwjgl.engine.gameObjects.holder;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.CameraModel;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.camera.camerTypes.ThirdPersonCamera;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.Light;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightTypes.basicLight.BasicLight;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;
import com.lwjgl.engine.openGL.shaders.entitiesShader.basicEntityShader.EntitiesShader;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

public class GameObjectHolder {
    private final EntitiesObjectHolder entitiesObjectHolder;
    private final TerrainObjectHolder terrainObjectHolder;
    private final LightsObjectHolder lightsObjectHolder;
    private final CameraModel camera;
    private final Light light;

    public GameObjectHolder(GlInputListener cameraGlInputListener) {
        this.entitiesObjectHolder = new EntitiesObjectHolder();
        this.terrainObjectHolder = new TerrainObjectHolder();
        this.lightsObjectHolder = new LightsObjectHolder(4);

        int index1 = EngineComponentsSharedObject.getEngineComponentSharedObject().getGlShaderController().addNewGlShaderAbstractProgram(new EntitiesShader());

        int modelId2 = getEntitiesObjectHolder().createNewGroupOfEntities(index1,"./res/assets/person.obj","stallTexture.png");
        EntityModel model = new EntityModel();
        model.setScale(0.02f);

        model.changeXPosition(0f);
        model.changeYPosition(0f);


        entitiesObjectHolder.addEntity(modelId2,model);

        model.setGlInputListener(new GlInputListener() {
            @Override
            public void listenForInputs(EntityModel entity, GlInputHandler glInputHandler) {
                if (glInputHandler.isKeyPressed(GLFW_KEY_W)){
                    model.moveDistance(0.08f);
                }
                if(glInputHandler.isKeyPressed(GLFW_KEY_S)){
                    model.increaseZPosition(-0.08f);
                }
                if(glInputHandler.isKeyPressed(GLFW_KEY_A)){
                    model.increaseYRotation(0.05f);
                }
                if(glInputHandler.isKeyPressed(GLFW_KEY_D)){
                    model.increaseYRotation(-0.05f);
                }
                if(glInputHandler.isKeyPressed(GLFW_KEY_P)){
                    model.increaseYPosition(0.05f);
                }
                if(glInputHandler.isKeyPressed(GLFW_KEY_L)){
                    model.increaseYPosition(-0.05f);
                }

            }
        });


        camera = new ThirdPersonCamera(0f,0.5f,model,cameraGlInputListener);
        ((ThirdPersonCamera)camera).setSensitivity(0.01f);

        lightsObjectHolder.addNewBasicLight(new BasicLight(new Vector3f(5,0.01f,5), new Vector3f(1f,1,1f),new Vector3f(1f,0f,1f)));
        lightsObjectHolder.addNewBasicLight(new BasicLight(new Vector3f(5,5,10), new Vector3f(1,1,1),new Vector3f(1f,0,0.01f)));
        lightsObjectHolder.addNewBasicLight(new BasicLight(new Vector3f(3,5,15), new Vector3f(1,1,1),new Vector3f(1f,0,0.01f)));
        lightsObjectHolder.addNewBasicLight(new BasicLight(new Vector3f(4,5,11), new Vector3f(1,1,1),new Vector3f(1f,0,0.01f)));

        light = new Light(12);
        EngineComponentsSharedObject.getEngineComponentSharedObject().setGameObjectHolder(this);
    }


    public EntitiesObjectHolder getEntitiesObjectHolder() {
        return entitiesObjectHolder;
    }

    public TerrainObjectHolder getTerrainObjectHolder() {
        return terrainObjectHolder;
    }

    public LightsObjectHolder getLightsObjectHolder() {
        return lightsObjectHolder;
    }

    public Light getLight() {
        return light;
    }

    public CameraModel getCamera() {
        return camera;
    }



}

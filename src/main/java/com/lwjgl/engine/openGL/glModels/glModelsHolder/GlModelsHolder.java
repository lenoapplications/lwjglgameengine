package com.lwjgl.engine.openGL.glModels.glModelsHolder;

import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;
import java.util.HashMap;

public class GlModelsHolder {
    private int key;
    private final HashMap<Integer, GlDataAbstract> glDataAbstractHashMap;

    public GlModelsHolder() {
        this.glDataAbstractHashMap = new HashMap<>();
        this.key = 0;
    }

    public int createNewModel(String objFilename, String textureFilename){
        GlDataAbstract glDataAbstract = new GlDataAbstract(false);
        glDataAbstract.initObjAndTexture(objFilename,textureFilename);

        return addNewModel(glDataAbstract);
    }
    public int createNewModel(String objFilename, String textureFilename,boolean transparency){
        GlDataAbstract glDataAbstract = new GlDataAbstract(transparency);
        glDataAbstract.initObjAndTexture(objFilename,textureFilename);

        return addNewModel(glDataAbstract);
    }


    public GlDataAbstract getModel(int key){
        return glDataAbstractHashMap.get(key);
    }

    private int addNewModel(GlDataAbstract glDataAbstract){
        if (!glDataAbstractHashMap.containsKey(key)){
            glDataAbstractHashMap.put(key,glDataAbstract);
            return key++;
        }
        return -1;
    }

}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainTextures.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainTextures.TerrainTexturesModel;
import com.lwjgl.engine.utils.resourceReader.pixelReader.PixelReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniqueVerticesMultipleTerrainTextures extends TerrainTexturesModel {
    private final int numberOfTextures;
    private final float texturesRatio;
    private final Map<String,Integer> textureIdentColor;


    public UniqueVerticesMultipleTerrainTextures(int size, int numberOfTextures) {
        super(size);
        this.numberOfTextures = numberOfTextures;
        this.textureIdentColor = new HashMap<>();
        this.texturesRatio = 1f/ (float)numberOfTextures;
    }

    public void generateTextures(String mapFilename,int rowSize, int columnSize ,String...colors){
        addColorsToMap(colors);
        readMap(mapFilename,rowSize,columnSize);
    }



    private void addColorsToMap(String...colors){
        int colorIndex = 0;
        for(String color : colors){
            textureIdentColor.put(color,colorIndex);
            ++colorIndex;
        }
    }


    private void readMap(String mapFilename,int rowSize,int columnSize){
        List<String> colorMap = PixelReader.readImageColorsFormTxtFile(mapFilename);

        for(int row = 0 ; row < rowSize ; ++row){
            for(int column = 0 ; column < columnSize ; ++column){
                String color = colorMap.get((row*columnSize)+column);
                addTextureCoordinates(textureIdentColor.get(color));
            }
        }
    }

    private void addTextureCoordinates(int textureIndex){
        float textureXCoordinate = texturesRatio * textureIndex;
        try {
            addNewValuesCurrentPointer(textureXCoordinate,1.0f);
            addNewValuesCurrentPointer(textureXCoordinate + texturesRatio,1.0f);
            addNewValuesCurrentPointer(textureXCoordinate + texturesRatio,0f);
            addNewValuesCurrentPointer(textureXCoordinate,0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args){
        UniqueVerticesMultipleTerrainTextures uniqueVerticesMultipleTextures = new UniqueVerticesMultipleTerrainTextures(2 * 2,2);

    }

}

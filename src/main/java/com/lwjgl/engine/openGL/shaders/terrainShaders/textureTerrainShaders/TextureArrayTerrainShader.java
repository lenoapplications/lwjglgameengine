package com.lwjgl.engine.openGL.shaders.terrainShaders.textureTerrainShaders;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;


public class TextureArrayTerrainShader extends GlShaderAbstractProgram {


    @Override
    public void init() {
        loadVertexShader("terrainShader","textureTerrainShaders","gls","ArrayTerrainShader.vert");
        loadFragmentShader("terrainShader","textureTerrainShaders","gls","ArrayTerrainShader.frag");
        createShaderProgram();
    }

    @Override
    protected void setupCustomUniformVariables() {
    }
}

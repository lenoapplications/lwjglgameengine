package com.lwjgl.engine.utils.resourceReader.pixelReader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class PixelReader {


    public static BufferedImage getBufferedImage(String filenamePath){
        try {
            BufferedImage image = ImageIO.read(new File(filenamePath));
            return image;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public static List<String> readImageColorsFormTxtFile(String filenamePath){
        try {
            List<String> pixels = new ArrayList<>();
            BufferedReader fileReader = new BufferedReader(new FileReader(filenamePath));
            fileReader.lines().forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    pixels.add(s);
                }
            });
            return pixels;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }
    public static List<String> getImagePixelColorsHex(String filenamePath,boolean startFromBottm){
        try {
            List<String> pixels = new ArrayList<>();
            BufferedImage image = ImageIO.read(new File(filenamePath));
            readImagePixelsHex(image,pixels,startFromBottm,true);
            return pixels;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public static void writeImagePixelColorsHex(String filenamePath,String toFile,boolean startFromBottom,boolean hexFormat){
        try {
            List<String> pixels = new ArrayList<>();
            BufferedImage image = ImageIO.read(new File(filenamePath));
            readImagePixelsHex(image,pixels,startFromBottom,hexFormat);
            FileWriter fileWriter = new FileWriter(new File(toFile));

            pixels.forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    try {
                        fileWriter.write(s+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    private static void readImagePixelsHex(BufferedImage image,List<String> pixels,boolean startFromBottom,boolean hexFormat){
        int y;
        int yEnd;
        int yIteration;
        if (startFromBottom){
            y = image.getHeight() - 1;
            yEnd = -1;
            yIteration = -1;
        }else{
            y = 0;
            yEnd = image.getHeight();
            yIteration = 1;
        }
        for (; y != yEnd ; y+=yIteration){
            for (int x = 0; x < image.getWidth(); x++){
                int clr = image.getRGB(x,y);
                int  red   = (clr & 0x00ff0000) >> 16;
                int  green = (clr & 0x0000ff00) >> 8;
                int  blue  =  clr & 0x000000ff;
                if(!hexFormat){
                    pixels.add(String.format("%d",clr));
                }else{
                    pixels.add(String.format("#%02x%02x%02x",red,green,blue));
                }
            }
        }
    }

    public static void main(String[] args){
        PixelReader.writeImagePixelColorsHex("./res/assets/ColorRbgTerrain.png","./res/assets/colorRgbTerrain.txt",true,false);
    }
}

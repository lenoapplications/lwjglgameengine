package com.lwjgl.engine.gui.window;

import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;
import org.lwjgl.system.MemoryStack;

import java.io.PrintStream;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window {
    private long window;
    private int width;
    private int height;
    public Callback callback;

    public Window(){
        this.window = -1;
    }

    public void initWindow(int width,int height,String title){
        this.width = width;
        this.height = height;

        GLFWErrorCallback.createPrint(System.err).set();

        if (glfwInit()){
            glfwDefaultWindowHints();
            glfwWindowHint(GLFW_VISIBLE,GLFW_FALSE);
            glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
            glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT,GLFW_TRUE);

            window = glfwCreateWindow(width,height,title,NULL,NULL);

            if (window != NULL){
                glfwSetKeyCallback(window, new GLFWKeyCallback() {
                    @Override
                    public void invoke(long window, int key, int scancode, int action, int mods) {
                        if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE){
                            glfwSetWindowShouldClose(window,true);
                        }
                    }
                });
                try (MemoryStack stack = MemoryStack.stackPush()){
                    IntBuffer pWidth = stack.mallocInt(1);
                    IntBuffer pHeight = stack.mallocInt(1);

                    glfwGetWindowSize(window,pWidth,pHeight);

                    GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

                    glfwSetWindowPos(window,(vidMode.width() - pWidth.get(0) / 2) / 2, (vidMode.height() - pHeight.get(0) / 2) / 2);
                }
                glfwMakeContextCurrent(window);
                glfwSwapInterval(1);
                glfwShowWindow(window);
                GL.createCapabilities();
                callback = GLUtil.setupDebugMessageCallback(new PrintStream(System.out));

                GlInputHandler glInputHandler = new GlInputHandler();

                glfwSetKeyCallback(window,glInputHandler.getGlfwKeyCallback());
                glfwSetMouseButtonCallback(window,glInputHandler.getGlfwMouseButtonCallback());
                glfwSetCursorPosCallback(window,glInputHandler.getGlfwCursorPosCallback());
                glfwSetScrollCallback(window,glInputHandler.getGlfwScrollCallback());


                saveToEngineComponentSharedObject();

            }
        }
    }

    private void saveToEngineComponentSharedObject(){
        EngineComponentsSharedObject.getEngineComponentSharedObject().setActiveWindow(this);
    }

    public void prepareWindow(){
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
        glClearColor(1.0f,1.0f,1.0f,1);
        glfwPollEvents();
    }
    public void swapBuffers(){
        glfwSwapBuffers(window);
    }


    public void close(){
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);        glEnable(GL_BLEND);


        glfwTerminate();
        glfwSetErrorCallback(null).free();
        window = -1;
    }

    public boolean shouldWindowClose(){
        try {
            return glfwWindowShouldClose(window);
        }catch (Exception e){
            return true;
        }
    }
    public boolean isWindowOpen(){
        return glfwGetWindowAttrib(window,GLFW_FOCUSED) == 1;
    }
    public long getWindow() {
        return window;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

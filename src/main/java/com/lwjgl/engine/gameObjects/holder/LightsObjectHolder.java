package com.lwjgl.engine.gameObjects.holder;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel.LightModel;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel.models.PointLight;
import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightTypes.basicLight.BasicLight;

public class LightsObjectHolder {
    private final int MAX_BASIC_LIGHTS;
    private final BasicLight[] basicLightModels;

    public LightsObjectHolder(int maxBasicLights){
        this.MAX_BASIC_LIGHTS = maxBasicLights;
        this.basicLightModels = new BasicLight[MAX_BASIC_LIGHTS];
    }

    public void addNewBasicLight(BasicLight basicLight){
        int index = 0;
        for(LightModel light : basicLightModels){
            if(light == null){
                basicLight.setIndexId(index);
                basicLightModels[index] = basicLight;
                break;
            }else{
                ++index;
            }
        }
    }
    public void addNewBasicLight(int index,BasicLight lightModel){
        basicLightModels[index] = lightModel;
    }
    public BasicLight[] getBasicLightModels(){
        return basicLightModels;
    }



    public void test(){
        for(int i = 0; i < MAX_BASIC_LIGHTS; ++i){
            System.out.println("index i "+i+"     "+basicLightModels[i]);
        }
    }

}

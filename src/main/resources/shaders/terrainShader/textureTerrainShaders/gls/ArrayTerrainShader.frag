#version 410 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightSource;
in vec3 toCameraVector;

out vec4 out_Color;

uniform sampler2D backgroundTexture;

uniform vec3 lightColor;
uniform float shineDamper;
uniform float reflectivity;




vec3 calculateLightBrightness(vec3 unitNormal,vec3 unitLightVector){
    float nDotl = dot(unitNormal,unitLightVector);
    float brightness = max(nDotl,0.3);
    vec3 diffuse = brightness * lightColor;
    return diffuse;
}

vec3 calculateSpecularLight(vec3 unitVectorToCamera,vec3 unitNormal,vec3 unitLightVector){
    vec3 lightDirection = -unitLightVector;
    vec3 reflectedLightDirection = reflect(lightDirection,unitNormal);
    float specularFactor = dot(reflectedLightDirection,unitVectorToCamera);
    specularFactor = max(specularFactor,0.0f);

    float dampedFactor = pow(specularFactor,shineDamper);
    vec3 finalSpecular = dampedFactor * lightColor;
    return finalSpecular;
}

bool checkIfTextureColorAlphaIsLowerThanHalf(float alpha){
    return alpha < 0.5;
}

//    function for checking what texture to display
//        backTextureAmount -> this field calculates how much of background texture should be displayed (if in blend map area is black then
//                             result is 1 and backgroundTexture will be fully displayed
vec4 calculateTextureColor(){
    vec2 tiledCoords = pass_textureCoords;
    vec4 backgroundTextureColor = texture(backgroundTexture, tiledCoords);

    return backgroundTextureColor;

}

void main(void){
    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitLightVector = normalize(toLightSource);
    vec3 unitVectorToCamera = normalize(toCameraVector);

    vec3 diffuse = calculateLightBrightness(unitNormal,unitLightVector);
    vec3 finalSpecular = calculateSpecularLight(unitVectorToCamera,unitNormal,unitLightVector);
    vec4 finalTextureColor = calculateTextureColor();

    out_Color = vec4(diffuse,1.0f) * finalTextureColor + vec4(finalSpecular,1.0f);
}
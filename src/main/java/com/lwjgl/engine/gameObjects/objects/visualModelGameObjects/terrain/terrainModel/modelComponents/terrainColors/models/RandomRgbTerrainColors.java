package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.models;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.TerrainColorsModel;

import java.util.Random;

public class RandomRgbTerrainColors extends TerrainColorsModel {
    private float a = 0.5f;

    public RandomRgbTerrainColors(int rowVertices, int columnVertices) {
        super(rowVertices, columnVertices);
    }

    public void generateColors(int rowVertices,int columnVertices){
        for(int row = 0; row < rowVertices ; ++row){
            for(int column = 0; column < columnVertices ; ++column){
                addColor(0.2f, new Random().nextFloat()/2,0.0f);
            }
        }

    }

}

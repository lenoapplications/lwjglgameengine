package com.lwjgl.engine.engineLoop;

import com.lwjgl.engine.engineLoop.loopModels.FrameRunner;
import com.lwjgl.engine.gui.window.Window;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;

public class EngineLoop {
    private boolean loopActive;
    private FrameRunner frameRunner;

    public EngineLoop(FrameRunner frameRunner){
        this.frameRunner = frameRunner;
        loopActive = false;
    }

    public void initLoop(){
        saveToEngineComponentSharedObject();
    }

    public void runEngineLoop(){
        Window window = EngineComponentsSharedObject.getEngineComponentSharedObject().getActiveWindow();
        FrameController frameController = new FrameController(60);
        if (window != null){
            activate();
            while(!window.shouldWindowClose()){
                frameController.nowTime();
                window.prepareWindow();
                frameRunner.runFrame();
                window.swapBuffers();
                frameController.passedTime();
                frameController.checkTimePerFrame();
                frameController.checkIfOneSecondPass();
            }
            deactivate();

        }
    }

    private boolean waitForWindowToOpen(Window window){
        try {
           Thread.sleep(10000);
           return true;
        }catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void activate(){
        loopActive = true;
    }
    private void deactivate(){
        loopActive = false;
    }

    private void saveToEngineComponentSharedObject(){
        EngineComponentsSharedObject.getEngineComponentSharedObject().setActiveEngineLoop(this);
    }


    public boolean isLoopActive() {
        return loopActive;
    }
}

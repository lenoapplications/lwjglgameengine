package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel.EntityModel;

import java.util.ArrayList;
import java.util.List;

public class EntityGroup {
    private final int indexGlShaderAbstractProgram;
    private final List<EntityModel> entityModelList;

    public EntityGroup(int index) {
        this.indexGlShaderAbstractProgram = index;
        this.entityModelList = new ArrayList<>();
    }

    public int getIndexGlShaderAbstractProgram() {
        return indexGlShaderAbstractProgram;
    }

    public List<EntityModel> getEntityModelList() {
        return entityModelList;
    }

}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.terrainUpdateInterface.TerrainUpdateInterface;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.visualModel.AbstractedGameObject;
import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;

public abstract class TerrainModel <A extends GlShaderAbstractProgram> extends AbstractedGameObject implements TerrainUpdateInterface<A> {
    protected final int glShaderAbstractProgramIndex;
    protected GlDataAbstract glDataAbstract;
    protected final float x;
    protected final float y;
    protected final float z;
    protected final float terrainWidth;
    protected final float terrainDepth;

    public TerrainModel(int glShaderAbstractProgramIndex,float x, float y, float z, float terrainWidth, float terrainDepth) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.terrainWidth = terrainWidth;
        this.terrainDepth = terrainDepth;
        this.glShaderAbstractProgramIndex = glShaderAbstractProgramIndex;
    }


    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        update((A) glShaderAbstractProgram);
        transform();
        loadUniforms(glShaderAbstractProgram);
    }

    protected void loadGlData(GlDataAbstract glDataAbstract){
        this.glDataAbstract = glDataAbstract;
    }

    protected void transform(){
        setTransformationMatrixIdentity();
        worldMatrix.translate(position);
        worldMatrix.scale(scale);
    }

    public GlDataAbstract getGlDataAbstract() {
        return glDataAbstract;
    }

    public int getIndexGlShaderAbstractProgram() {
        return glShaderAbstractProgramIndex;
    }
}

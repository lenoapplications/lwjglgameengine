#version 410 core

uniform sampler2D textureSampler;
uniform vec3 lightColors[4];
uniform float shineDamper;
uniform float reflectivity;

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 lightDifferenceVectors[4];
in vec3 toCameraVector;

out vec4 out_Color;

vec3 calculateLightBrightness(vec3 unitNormal,vec3 unitLightVector,vec3 lightColor){
    float nDotl = dot(unitNormal,unitLightVector);
    float brightness = max(nDotl,0.3);
    vec3 diffuse = brightness * lightColor;
    return diffuse;
}

vec3 calculateSpecularLight(vec3 unitVectorToCamera,vec3 unitNormal,vec3 unitLightVector,vec3 lightColor){
    vec3 lightDirection = -unitLightVector;
    vec3 reflectedLightDirection = reflect(lightDirection,unitNormal);
    float specularFactor = dot(reflectedLightDirection,unitVectorToCamera);
    specularFactor = max(specularFactor,0.0f);

    float dampedFactor = pow(specularFactor,shineDamper);
    vec3 finalSpecular = dampedFactor * lightColor;
    return finalSpecular;
}

bool checkIfTextureColorAlphaIsLowerThanHalf(float alpha){
    return alpha < 0.5;
}

vec4 calculateTotalDiffuseAndSpecularLightning(vec4 textureColor){
    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitVectorToCamera = normalize(toCameraVector);

    vec3 totalDiffuse = vec3(0,0,0);
    vec3 totalSpecularLighting = vec3(0,0,0);

    for(int light = 0 ; light < 4 ; ++light){
        vec3 unitLightVector = normalize(lightDifferenceVectors[light]);
        vec3 lightColor = lightColors[light];
        totalDiffuse += calculateLightBrightness(unitNormal,unitLightVector,lightColor);
        totalSpecularLighting += calculateSpecularLight(unitVectorToCamera,unitNormal,unitLightVector,lightColor);
    }
    return vec4(totalDiffuse,1) * textureColor * vec4(totalSpecularLighting,1.0);
}

void main(void){
    vec4 textureColor = texture(textureSampler,pass_textureCoords);
    out_Color = calculateTotalDiffuseAndSpecularLightning(textureColor);
}


package com.lwjgl.engine.openGL.glModels.glAbstractData.glDataIdentHolder;

public class GlDataIdentHolder {
    protected int vaoId;
    private int verticesVboId;
    private int textureVboId;
    private int indicesVboId;
    private int normalsVboId;
    private int colorsVboId;


    public void setVaoId(int vaoId) {
        this.vaoId = vaoId;
    }

    public void setVerticesVboId(int verticesVboId) {
        this.verticesVboId = verticesVboId;
    }

    public void setTextureVboId(int textureVboId) {
        this.textureVboId = textureVboId;
    }

    public void setIndicesVboId(int indicesVboId) {
        this.indicesVboId = indicesVboId;
    }
    public void setNormalsVboId(int normalsVboId){
        this.normalsVboId = normalsVboId;
    }

    public void setColorsVboId(int colorsVboId) {
        this.colorsVboId = colorsVboId;
    }

    public int getVerticesVboId() {
        return verticesVboId;
    }

    public int getTextureVboId() {
        return textureVboId;
    }

    public int getIndicesVboId() {
        return indicesVboId;
    }

    public int getNormalsVboId() {
        return normalsVboId;
    }

    public int getColorsVboId() {
        return colorsVboId;
    }

    public int getVaoId() {
        return vaoId;
    }
}

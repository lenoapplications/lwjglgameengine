package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainTypes.colorTerrainTypes.lowColorPolygonTerrainType;

import com.lwjgl.engine.openGL.glModels.glAbstractData.GlDataAbstract;


public class LowColorPolygonTerrainDataAbstract extends GlDataAbstract {


    public LowColorPolygonTerrainDataAbstract() {
        super(true);
    }

    @Override
    protected void activateAndBindBaseTexture() {
        if(hasTransparency()){
            disableCulling();
        }
    }

}

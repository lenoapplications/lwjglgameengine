package com.lwjgl.engine.utils.math.mathConstruction.vectors;

public class Vector3d {
    private float x;
    private float y;
    private float z;
    private float w;


    public Vector3d(float x,float y,float z,float w){
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    public void increaseX(float x){
        this.x +=x;
    }
    public void increaseY(float y){
        this.y +=y;
    }
    public void increaseZ(float z){
        this.z +=z;
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setW(float w) {
        this.w = w;
    }
    public float getW() {
        return w;
    }


    @Override
    public String toString() {
        System.out.println("{");
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(w);
        System.out.println("}");

        return "sdada";
    }
}

package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainTypes.colorTerrainTypes.lowColorPolygonTerrainType;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.TerrainModel;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainColors.models.LowPolygonTerrainColors;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainIndices.models.SharedVerticesTerrainIndices;
import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.terrain.terrainModel.modelComponents.terrainVertices.models.SharedVerticesTerrain;
import com.lwjgl.engine.openGL.shaders.terrainShaders.colorTerrainShaders.LowColorPolygonTerrainShader;
import com.lwjgl.engine.utils.resourceReader.pixelReader.PixelReader;
import org.joml.Vector3f;

import java.util.Random;

public class LowColorPolygonTerrainType extends TerrainModel<LowColorPolygonTerrainShader> {
    private final Vector3f sunSourceLightPosition;
    private final Vector3f sunSourceLightColor;


    public LowColorPolygonTerrainType(int glShaderAbstractProgramIndex, float x, float y, float z, float terrainWidth, float terrainDepth) {
        super(glShaderAbstractProgramIndex, x, y, z, terrainWidth, terrainDepth);
        this.sunSourceLightPosition = new Vector3f(4f,0.2f,5f);
        this.sunSourceLightColor = new Vector3f(1f,1f,1);
    }


    public void generateTerrain(String mapFilename,String heighMapFilename, int rowVertices, int columnVertices,float maxHeight){
        SharedVerticesTerrain sharedVerticesTerrain = new SharedVerticesTerrain(rowVertices,columnVertices,x,y,z,terrainWidth,terrainDepth);
        SharedVerticesTerrainIndices sharedVerticesTerrainIndices = new SharedVerticesTerrainIndices((rowVertices - 1) * (columnVertices - 1));
        LowPolygonTerrainColors sharedRgbTerrainColors = new LowPolygonTerrainColors(rowVertices,columnVertices);

        sharedVerticesTerrain.generateTerrainModel();
        sharedVerticesTerrainIndices.generateSharedIndices(rowVertices,columnVertices);
        sharedRgbTerrainColors.generateColors(PixelReader.readImageColorsFormTxtFile(mapFilename));

        loadGlDataAbstract(sharedVerticesTerrain.getAllVertices(),sharedRgbTerrainColors.getAllColors(),sharedVerticesTerrainIndices.getAllIndices());
    }

    private void loadGlDataAbstract(float[] vertices,float[] colors,int[] indices){
        LowColorPolygonTerrainDataAbstract lowColorPolygonTerrainDataAbstract = new LowColorPolygonTerrainDataAbstract();
        lowColorPolygonTerrainDataAbstract.initObjAndColor(vertices,colors,indices);
        loadGlData(lowColorPolygonTerrainDataAbstract);
    }

    @Override
    public void update(LowColorPolygonTerrainShader glShaderAbstractProgram) {
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.SUN_COLOR,sunSourceLightColor);
        glShaderAbstractProgram.loadVector3f(glShaderAbstractProgram.SUN_SOURCE,sunSourceLightPosition);
        changeSunPosition();
    }



    private void changeSunColor(){
    }
    private void changeSunPosition(){
    }

}

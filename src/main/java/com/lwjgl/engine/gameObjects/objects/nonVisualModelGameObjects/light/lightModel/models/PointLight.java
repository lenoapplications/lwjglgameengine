package com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel.models;

import com.lwjgl.engine.gameObjects.objects.nonVisualModelGameObjects.light.lightModel.LightModel;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import org.joml.Vector3f;

public abstract class PointLight extends LightModel {
    protected final Vector3f attenuation;

    public PointLight(Vector3f lightPosition, Vector3f lightColor,Vector3f attenuation) {
        super(lightPosition, lightColor);
        this.attenuation = attenuation;
    }
}

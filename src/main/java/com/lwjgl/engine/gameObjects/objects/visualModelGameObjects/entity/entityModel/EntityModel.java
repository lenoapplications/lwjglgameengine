package com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityModel;

import com.lwjgl.engine.gameObjects.objects.visualModelGameObjects.entity.entityTransformation.Transformation;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputHandler;
import com.lwjgl.engine.openGL.glModels.glInputHandler.GlInputListener;
import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.GlShaderAbstractProgram;
import com.lwjgl.engine.sharedObject.engineComponentsSharedObject.EngineComponentsSharedObject;


public class EntityModel extends Transformation {
    private boolean active;
    private GlInputListener glInputListener;
    private final GlInputHandler glInputHandler;

    public EntityModel(){
        glInputHandler = EngineComponentsSharedObject.getEngineComponentSharedObject().getGlInputHandler();
    }



    public void activate(){
        active = true;
    }
    public void deactivate(){
        active = true;
    }
    public boolean isActive() {
        return active;
    }
    public void setGlInputListener(GlInputListener glInputListener) {
        this.glInputListener = glInputListener;
    }

    @Override
    public void transformAndLoadUniforms(GlShaderAbstractProgram glShaderAbstractProgram) {
        if (glInputListener != null){
            glInputListener.listenForInputs(this,glInputHandler);
        }
        transformEntityModel();
        loadUniforms(glShaderAbstractProgram);
    }

}

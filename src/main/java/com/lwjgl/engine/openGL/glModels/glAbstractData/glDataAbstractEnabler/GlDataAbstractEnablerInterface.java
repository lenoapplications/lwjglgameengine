package com.lwjgl.engine.openGL.glModels.glAbstractData.glDataAbstractEnabler;

public abstract class GlDataAbstractEnablerInterface {
    private final int[] attributeIndexes;

    public GlDataAbstractEnablerInterface(int[] attributeIndexes){
        this.attributeIndexes = attributeIndexes;
    }

    public abstract void enableVertex();
    public abstract void disableVertex();
}

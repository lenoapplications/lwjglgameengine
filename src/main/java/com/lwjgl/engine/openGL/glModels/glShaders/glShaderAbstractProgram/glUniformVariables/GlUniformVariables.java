package com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.glUniformVariables;

import com.lwjgl.engine.openGL.glModels.glShaders.glShaderAbstractProgram.glShaderIdentHolder.GlShaderIdentHolder;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.HashMap;
import static org.lwjgl.opengl.GL20.*;

public abstract class GlUniformVariables extends GlShaderIdentHolder {
    private final int MAX_BASIC_LIGHTS = 4;
    private final HashMap<String,Integer> locationIds;
    public final String WORLD_MATRIX = "worldMatrix";
    public final String PROJECTION_MATRIX = "projectionMatrix";
    public final String VIEW_MATRIX = "viewMatrix";
    public final String SHINE_DAMPER = "shineDamper";
    public final String REFLECTIVITY = "reflectivity";
    public final String[] BASIC_LIGHTS = new String[MAX_BASIC_LIGHTS];
    public final String[] BASIC_LIGHTS_COLORS = new String[MAX_BASIC_LIGHTS];
    public final String[] BASIC_LIGHTS_ATTENUATION= new String[MAX_BASIC_LIGHTS];

    protected GlUniformVariables() {
        locationIds = new HashMap<>();
    }

    protected void setupBasicUniformVariables(){
        setNewUniformLocation(WORLD_MATRIX);
        setNewUniformLocation(PROJECTION_MATRIX);
        setNewUniformLocation(VIEW_MATRIX);
        setNewUniformLocation(SHINE_DAMPER);
        setNewUniformLocation(REFLECTIVITY);

        for(int light = 0; light < MAX_BASIC_LIGHTS ; light++){
            BASIC_LIGHTS[light] = "lightPositions[" + light + "]";
            BASIC_LIGHTS_COLORS[light] = "lightColors["+light +"]";
            BASIC_LIGHTS_ATTENUATION[light] = "lightAttenuation["+light +"]";

            setNewUniformLocation(BASIC_LIGHTS[light]);
            setNewUniformLocation(BASIC_LIGHTS_COLORS[light]);
            setNewUniformLocation(BASIC_LIGHTS_ATTENUATION[light]);

        }
    }

    protected abstract void setupCustomUniformVariables();

    protected boolean setNewUniformLocation(String locationName){
        int id = glGetUniformLocation(shaderProgramId,locationName);
        locationIds.put(locationName,id);
        return true;
    }

    public void loadFloat(String locationName,float value){
        glUniform1f(locationIds.get(locationName),value);
    }
    public void loadInt(String locationName,int value){
        glUniform1i(locationIds.get(locationName),value);
    }
    public void loadBoolean(String locationName,boolean value){
        float toLoad = (value) ? 1 : 0;
        glUniform1f(locationIds.get(locationName),toLoad);
    }
    public void loadVector3f(String locationName, Vector3f vector3f){
        glUniform3f(locationIds.get(locationName),vector3f.x,vector3f.y,vector3f.z);
    }
    public void loadMatrix4f(String locationName, Matrix4f matrix4f){
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(16);
        matrix4f.get(floatBuffer);
        glUniformMatrix4fv(locationIds.get(locationName),false,floatBuffer);
    }

}
